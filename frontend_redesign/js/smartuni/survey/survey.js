function addLink(question) {
    let element = $('#question-template .new-question-parameters').clone();
    $('#question-container').append(element);
}

function saveSubmit() {
    let payload = {
        name: $('#test-name').val(),
        links: [],
    };

    $('#question-container input')
        .serializeArray()
        .forEach((link) => payload.links.push(link.value));

    axios
        .post(`/public/submit`, payload)
        .then((res) => {
            if (res.status == 201) location.href = '/public/success';
        })
        .catch(Notification.unknownError);
}

function showLinks(submitId) {
    const modal = $('#links-modal');
    modal.empty();

    axios
        .get(`/admin/links?submit_id=${submitId}`)
        .then((res) => {
            modal.append(res.data);
        })
        .catch(Notification.unknownError);
}

function reset() {
    axios
        .delete('/admin/reset')
        .then((res) => location.reload())
        .catch(Notification.unknownError);
}
