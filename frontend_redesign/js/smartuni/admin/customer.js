function removeCustomer(id, currentCustomerId) {
    if (id === currentCustomerId) {
        Notification.error('Az intézmény nem törölhető, mert épp ez van kiválasztva!')
    } else {
        if (confirm('Biztosan törölni szeretné az intézményt?')) {
            axios.delete(`/admin/customers/${id}`)
                .then(res => {
                    if (res.status == 200) location.reload()
                })
                .catch(err => {
                    Notification.error('Hiba történt az intézmény törlése során.')
                    console.error('Failed to delete customer: ' + id)
                })
        }
    }
}