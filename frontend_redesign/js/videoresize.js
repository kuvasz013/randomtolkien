function initVideoResize() {
  const videoAndConrolPanels = document.querySelectorAll(
    ".video-control-panel"
  );
  function resizeMyVideo(videoAndConrolPanel) {
    let video = videoAndConrolPanel.querySelector(".lession-video");
    let controlPanel = videoAndConrolPanel.querySelector(
      ".lession-controlpanel"
    );
    let mainPanel = videoAndConrolPanel.querySelector(".lession-mainpanel");

    let videoSides = video.dataset.screenratio.split(":");
    let videoScreenRatio = videoSides[0] / videoSides[1];

    let wWidth = window.innerWidth;
    let wHeight = window.innerHeight;

    function calcMaxWidth() {
      let controlPanelWidth = controlPanel.offsetWidth;
      let borderAround =
        parseInt(getStyleValue(videoAndConrolPanel, "padding-left")) +
        parseInt(getStyleValue(videoAndConrolPanel, "padding-right"));

      let mainPanelMargin = parseInt(getStyleValue(mainPanel, "margin-right"));
      let videoMaxWidth =
        wWidth - controlPanelWidth - mainPanelMargin - borderAround;

      return videoMaxWidth;
    }

    function calcMaxHeight() {
      let title = videoAndConrolPanel.querySelector(".main-title");
      let titleHeight =
        title.offsetHeight +
        parseInt(getStyleValue(title, "margin-top")) +
        parseInt(getStyleValue(title, "margin-bottom"));

      let control = videoAndConrolPanel.querySelector(".control-container");
      let controlHeight =
        control.offsetHeight +
        parseInt(getStyleValue(control, "margin-top")) +
        parseInt(getStyleValue(control, "margin-bottom"));

      let borderAround =
        parseInt(getStyleValue(videoAndConrolPanel, "padding-top")) +
        parseInt(getStyleValue(videoAndConrolPanel, "padding-bottom"));

      let videoMaxWidth = wHeight - titleHeight - controlHeight - borderAround;

      return videoMaxWidth;
    }

    let calcVideoHeight = video.offsetWidth / videoScreenRatio;
    let calcMaxVideoHeight = calcMaxHeight();

    if (calcVideoHeight > calcMaxVideoHeight) {
      console.log("nagyobb");
      video.style.height = calcMaxVideoHeight + "px";
      video.style.width = calcMaxVideoHeight * videoScreenRatio + "px";
    } else {
      video.style.width = calcVideoHeight * videoScreenRatio + "px";
      let videoHeight = video.offsetWidth / videoScreenRatio;
      video.style.height = calcVideoHeight + "px";
      console.log("kisebb");
    }

    /*console.log(calcMaxVideoWidth * videoScreenRatio);
    if (calcMaxVideoWidth < calcVideoHeight) {
      video.style.width = "";
    } else {
      video.style.width = calcMaxVideoWidth * videoScreenRatio;
    }

    let videoHeight = video.offsetWidth / videoScreenRatio;
    video.style.height = videoHeight + "px";*/
  }

  function resizeMyVideos() {
    for (let videoAndConrolPanel of videoAndConrolPanels) {
      resizeMyVideo(videoAndConrolPanel);
    }
  }

  document.onload = resizeMyVideos();
  window.addEventListener("resize", resizeMyVideos);
}
