function setOnOff(dom, offIsDefault = null) {
    let state;
    offIsDefault ? (state = { initial: 'on', poked: 'off' }) : (state = { initial: 'off', poked: 'on' });
    if (dom.classList.contains('off')) {
        dom.classList.remove(state.initial);
        dom.classList.add(state.poked);
    } else {
        dom.classList.add(state.initial);
        dom.classList.remove(state.poked);
    }
}

function getStyleValue(dom, cssRule) {
    var strValue = '';
    if (document.defaultView && document.defaultView.getComputedStyle) {
        strValue = document.defaultView.getComputedStyle(dom, '').getPropertyValue(cssRule);
    } else if (dom.currentStyle) {
        cssRule = cssRule.replace(/\-(\w)/g, function (strMatch, p1) {
            return p1.toUpperCase();
        });
        strValue = dom.currentStyle[cssRule];
    }
    return strValue;
}

function deleteEntity(endpoint, entity, btn, currentEntityId) {
    const entity_id = $(btn).data('url-param');

    if (currentEntityId && currentEntityId == entity_id) {
        return Notification.error('A kiválaszott elem nem törölhető!');
    }

    axios
        .delete(`/${endpoint}/${entity_id}`)
        .then((res) => {
            Notification.success('Sikeres törlés!', 'A kiválasztott elemet töröltük.');
            $(`#${entity}-${entity_id}`).remove();
            if (entity == 'course') {
                $(`.lesson-${entity_id}`).remove();
            }
        })
        .catch(Notification.unknownError)
        .then((res) => closeModals($('.modal-cover')[0], $('.modal-screen').toArray()));
}

function deleteEntityNoModal(url, row) {
    axios
        .delete(url)
        .then((res) => {
            Notification.success('Sikeres törlés!', 'A kiválasztott elemet töröltük.');
            $(row).remove();
        })
        .catch(Notification.unknownError);
}

function createEntity(formId, url, redirectUrl) {
    let payload = {};
    $(`${formId} input, ${formId} textarea, ${formId} select`)
        .serializeArray()
        .forEach((entry) => {
            payload[entry.name] = entry.value;
        });

    axios
        .post(url, payload)
        .then((res) => {
            if (redirectUrl) {
                location.href = redirectUrl.replace('%NEWID%', res.data.id);
            } else {
                location.reload();
            }
        })
        .catch(Notification.unknownError);
}

function updateEntity(formId, url) {
    let payload = {};
    $(`${formId} input, ${formId} textarea`)
        .serializeArray()
        .forEach((entry) => {
            payload[entry.name] = entry.value;
        });

    axios
        .put(url, payload)
        .then((res) => {
            Notification.success('Sikeres mentés!');
            location.reload();
        })
        .catch(Notification.unknownError)
        .then((res) => closeModals($('.modal-cover')[0], $('.modal-screen').toArray()));
}
