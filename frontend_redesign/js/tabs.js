function tabInit(tabItem, tabContent, activeClassName = "active") {
  const tabs = document.querySelectorAll(tabItem);
  const actionAreas = document.querySelectorAll(".action-area-wrapper");
  const tabContents = document.querySelectorAll(tabContent);
  tabs.forEach(function (tab) {
    let button = tab.querySelector(".tab-button");
    button.addEventListener("click", (e) => {
      e.preventDefault();
      setActive(tabs, tab);
      let tabName = tab.dataset.target;
      let content = document.querySelector(tabName);
      setActiveActionArea(tabName);
      setActive(tabContents, content);
    });
  });

  function setActive(allDoms, selectedDom) {
    allDoms.forEach(function (dom) {
      dom.classList.remove(activeClassName);
    });
    selectedDom.classList.toggle(activeClassName);
  }

  function setActiveActionArea(tabName) {
    if (actionAreas) {
      let allActionAreas = document.querySelectorAll(".action-area");
      let labels = ["-action", "-action-top", "-action-bottom"];
      allActionAreas.forEach((actionArea) => {
        actionArea.classList.remove(activeClassName);
      });
      for (let label of labels) {
        let content = document.querySelector(tabName + label);
        if (content) {
          content.classList.add(activeClassName);
        }
      }
    }
  }
}
