function modalInit() {
    const modalBtns = document.querySelectorAll('.open-modal');
    const closeModalBtns = document.querySelectorAll('.close-modal');
    const modalScreens = document.querySelectorAll('.modal-screen');
    const modalCover = document.querySelector('.modal-cover');

    let modals = new Set();

    modalBtns.forEach(function (modalBtn) {
        let target = modalBtn.dataset.target;
        let modal = document.querySelector(target);
        let urlParam = $(modalBtn).data('url-param');
        let fillModalWith = $(modalBtn).data('fill-modal-with');

        modalBtn.addEventListener('click', function (e) {
            e.preventDefault;
            closeModals(modalCover, modalScreens);
            modal.classList.add('show');
            modalCover.classList.add('show');

            // If modal has a targeted link button, set button's target URL
            if (urlParam) $(modal).trigger('target-modal', urlParam);
            // Fill modal if it is to be filled with backend rendered content
            else if (fillModalWith) {
                $(modal).empty();
                axios
                    .get(fillModalWith)
                    .then((res) => $(modal).append(res.data))
                    .catch((err) => {
                        if (err.response.status == 404) Notification.error('A keresett célpont nem található!');
                        else Notification.unknownError(err);

                        closeModals(modalCover, modalScreens);
                    });
            }
        });

        if (urlParam || fillModalWith) modals.add(target);
    });

    // Initialize targeted modal buttons
    modals.forEach((modal) => {
        $(modal).on('target-modal', function (e, urlParam) {
            $(modal).find('.modal-url-button').data('url-param', urlParam);
        });
    });

    closeModalBtns.forEach(function (closeModalBtn) {
        closeModalBtn.addEventListener('click', function (e) {
            e.preventDefault;
            closeModals(modalCover, modalScreens);
        });
    });
}

function closeModals(cover, screens) {
    cover.classList.remove('show');
    screens.forEach(function (screen) {
        screen.classList.remove('show');
    });
}
