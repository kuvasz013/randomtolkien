function collapseInit(collapseBtn) {
  const collapse = document.querySelectorAll(collapseBtn);
  let openbtn;
  let closebtn;
  let target;
  if (!collapse.length == 0) {
    collapse.forEach(function (button) {
      let targetName = button.dataset.target;
      let action = button.dataset.action;
      if (action === "open") {
        openbtn = button;
      }
      if (action === "close") {
        closebtn = button;
      }
      target = document.querySelector(targetName);
    });

    function resetCollapse() {
      if (document.documentElement.clientWidth <= 641) {
        closebtn.style.display = "none";
        openbtn.style.display = "block";
        target.style.display = "none";
      } else {
        target.style.display = "flex";
        closebtn.style.display = "none";
        openbtn.style.display = "none";
      }
    }

    resetCollapse();
    window.addEventListener("resize", () => resetCollapse());

    closebtn.addEventListener("click", function (e) {
      e.preventDefault;
      openbtn.style.display = "block";
      closebtn.style.display = "none";
      target.style.display = "none";
    });
    openbtn.addEventListener("click", function (e) {
      e.preventDefault;
      openbtn.style.display = "none";
      closebtn.style.display = "block";
      target.style.display = "flex";
    });
  }
}
