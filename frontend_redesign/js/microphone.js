function micInit(microphoneClass) {
  const microphones = document.querySelectorAll(microphoneClass);
  microphones.forEach(function (microphone) {
    setOnOff(microphone, true);
    microphone.addEventListener("click", (e) => {
      e.preventDefault();
      setOnOff(microphone);
    });
  });
}
