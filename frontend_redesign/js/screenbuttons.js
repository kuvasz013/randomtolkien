function buttonInit(buttonContainer) {
  const roundButtons = document.querySelectorAll(buttonContainer);
  roundButtons.forEach(function (button) {
    setOnOff(adjustParent(button));

    button.addEventListener("click", function (e) {
      e.preventDefault();
      actLinkedButton(button);
      performCommands(adjustParent(button).dataset.target);
      setOnOff(adjustParent(button));
    });
  });

  function performCommands(targets) {
    if (targets) {
      let commands = targets.split("/");
      for (let command of commands) {
        let task = command.split(":");
        document.querySelector(task[0]).classList.toggle(task[1]);
      }
    }
  }

  function actLinkedButton(button) {
    let buttonTypes = ["desktop", "tablet", "phone"];
    let buttonId = button.id;
    let thisButton = buttonId.split("-").pop();
    let linkedButtonsTypes = buttonTypes.filter(
      (buttonType) => buttonType !== thisButton
    );
    for (let linkedButtonType of linkedButtonsTypes) {
      let linkedButtonId = "#" + buttonId.replace(thisButton, linkedButtonType);
      let linkedButton = document.querySelector(linkedButtonId);
      if (linkedButton) {
        setOnOff(adjustParent(linkedButton));
      }
    }
  }

  function adjustParent(dom, ifNameIs = "round-button") {
    let actingElement;
    dom.classList.contains(ifNameIs)
      ? (actingElement = dom.parentNode)
      : (actingElement = dom);
    return actingElement;
  }
}
