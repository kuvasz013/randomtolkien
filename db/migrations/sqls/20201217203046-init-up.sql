create table submits (
	id serial primary key,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
	name varchar(128) not null
);

create table links (
    id serial primary key,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    submit_id integer references submits(id) DEFERRABLE INITIALLY DEFERRED,
    url varchar(256) not null
);
