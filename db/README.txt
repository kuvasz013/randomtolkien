Parancsok: https://db-migrate.readthedocs.io/en/latest/Getting%20Started/commands/

Új migration step hozzáadása:

Global install esetén:
$ db-migrate create add-emails

Local esetén:
$ node ../node_modules/db-migrate/bin/db-migrate create STEPNAME

Teljes migrate futtatás:
$ npm run db:migrate