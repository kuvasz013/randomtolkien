
let statusUpdateInterval = null;
let status_wrapper = null;

function startStreamServerStatusUpdate() {
    status_wrapper = $('#stream_server_status');
    updateStreamServerStatus();
    statusUpdateInterval = setInterval(updateStreamServerStatus, 10000);
}

function updateStreamServerStatus() {

    axios.get('/wowza/status', {})
        .then(function (response) {

            if (response.data == 'online') {
                status_wrapper.addClass('hidden');
            } else {
                status_wrapper.removeClass('hidden');
            }

        })
        .catch(function (error) {
            console.log(error);
        });

}

function showCourseEdit() {
    $('#course_edit').removeClass('hidden');
    $('#course_edit_btn').hide();
}

function hideCourseEdit() {
    $('#course_edit').addClass('hidden');
    $('#course_edit_btn').show();
}



function showLessonEdit() {
    $('#lesson_edit').removeClass('hidden');
    $('#lesson_edit_btn').hide();
}

function hideLessonEdit() {
    $('#lesson_edit').addClass('hidden');
    $('#lesson_edit_btn').show();
}




/* Lesson content */

function addSurveyContent(btn) {

    let selected_surveys = [];

    $('input.survey-add-select:checked').each(function () {
        selected_surveys.push($(this).val());
    });

    axios.post('/lessons/' + lesson_id + '/content', {
        add_surveys: selected_surveys
    })
        .then(function (resp) {
            $(btn).closest('.modal').modal('hide');
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}

function removeSurveyContent(btn) {

    let td = $(btn).closest('.content-survey');
    let survey_id = td.attr('data-id');

    axios.post('/lessons/' + lesson_id + '/content', {
        remove_surveys: [survey_id]
    })
        .then(function (resp) {
            // td.remove();
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}

function addPollContent(btn) {

    let selected_polls = [];

    $('input.poll-add-select:checked').each(function () {
        selected_polls.push($(this).val());
    });

    axios.post('/lessons/' + lesson_id + '/content', {
        add_polls: selected_polls
    })
        .then(function (resp) {
            $(btn).closest('.modal').modal('hide');
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}

function removePollContent(btn) {

    let td = $(btn).closest('.content-poll');
    let poll_id = td.attr('data-id');

    axios.post('/lessons/' + lesson_id + '/content', {
        remove_polls: [poll_id]
    })
        .then(function (resp) {
            // td.remove();
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}

function addLinkContent(btn) {

    let selected_links = [];

    $('input.link-add-select:checked').each(function () {
        selected_links.push($(this).val());
    });

    axios.post('/lessons/' + lesson_id + '/content', {
        add_links: selected_links
    })
        .then(function (resp) {
            $(btn).closest('.modal').modal('hide');
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}

function removeLinkContent(btn) {

    let td = $(btn).closest('.content-link');
    let link_id = td.attr('data-id');

    axios.post('/lessons/' + lesson_id + '/content', {
        remove_links: [link_id]
    })
        .then(function (resp) {
            // td.remove();
            reloadContentList();
        })
        .catch(function (err) {
            console.log(err);
        });

}


function showContentModal() {

    axios.get('/lessons/' + lesson_id + '/content')
        .then(function (resp) {
            let modal = $(resp.data);

            $(document.body).append(modal);
            modal.modal('show');
            modal.on('hidden.bs.modal', function (e) {
                $(e.target).remove();
            });

        });


}

function removeModal(btn, clearBody) {
    $(btn).closest('.modal').modal('hide');

    if (clearBody) {
        $(btn).closest('.modal').find('.modal-body').html('');
    }
}

function openEmbedPlayer(link) {
    let lesson_id = $(link).data('lesson'),
        session = $(link).data('session');

    $('#play_vod_modal').modal('show');

    $('#play_vod_modal .modal-body').html(`<iframe src="/lessons/${lesson_id}/embed_player/${session}" style="width:100%; height: 500px"></iframe>`);
    
}


function reloadContentList() {
    axios.get('/lessons/' + lesson_id + '/contentlist')
        .then(function (resp) {
            $('#content_list_body').html(resp.data);
        });
}


function showCreateLinkModal() {
    $('#create_link_modal').modal('show');
    $('#create_link_modal').on('hidden.bs.modal', function (e) {
        $(e.target).remove();
    });
}

function addFileContent(input) {
    const formData = new FormData();
    input.forEach(file => formData.append('file', file));
    let config = { onUploadProgress: progress => console.log(Math.round(progress.loaded / progress.total * 100)) };

    axios.post('/lessons/' + lesson_id + '/attachments', formData, config)
        .then(function (resp) {
            $(input).closest('.modal').modal('hide');
            reloadContentList();
        });
}

function uploadFile(fileList, url) {
    let template = $('#attachment_template .content-list-item');
    
    Array.from(fileList).forEach(file => {
        let item = $(template).clone();
        $(item).removeClass('fade');
        $(item).find('p').append(file.name);        

        $('.content-list').find('#no-attachments').remove();
        $('.content-list').prepend(item);

        const formData = new FormData();
        formData.append('file', file);
        
        let config = { 
            onUploadProgress: progress => {
                console.log('on progress');
                let percentage = Math.round(progress.loaded / progress.total * 100);
                $(item).find('.progress-bar').text(`${percentage}%`);
                $(item).find('.progress-bar').width(`${percentage}%`);
            }
        };

        axios.post(url, formData, config)
            .then(resp => {
                if (resp.data) {
                    $(item).replaceWith(resp.data);
                }
            })
            .catch(err => Notification.error('Sikertelen feltöltés!'));
    });
}

function removeAttachmentContent(btn) {
    let container = $(btn).closest('.content-attachment'),
        attachment_id = container.attr('data-id'),
        course_id = container.attr('data-course-id');

    axios.delete('/courses/' + course_id + '/attachments/' + attachment_id)
        .then(function (resp) {
            reloadContentList();
        });
}


// Stream admin

function startInstance(btn) {

    let machine_type = $('#machine_type').val();

    $(btn).find('.title').html('Indítás folyamatban...');
    $(btn).find('.spinner-border').removeClass('hidden');
    $(btn).attr('disabled', 'disabled');

    axios.post('/admin/startinstance', { machine_type: machine_type }).then(function (resp) {
        location.reload();
    });

}

function deleteInstance(btn) {

    $(btn).find('.title').html('Törlés folyamatban...');
    $(btn).find('.spinner-border').removeClass('hidden');
    $(btn).attr('disabled', 'disabled');

    axios.post('/admin/deleteinstance', {}).then(function (resp) {
        location.reload();
    });

}

$(function () {
    if (typeof(flash_messages) != 'undefined') {
        if (flash_messages['error']) {
            for (let i in flash_messages['error']) {
                let msg = flash_messages['error'][i];
                Notification.error(msg);
            }
        }
        if (flash_messages['success']) {
            for (let i in flash_messages['success']) {
                let msg = flash_messages['success'][i];
                Notification.success(msg);
            }
        }
        if (flash_messages['info']) {
            for (let i in flash_messages['info']) {
                let msg = flash_messages['info'][i];
                Notification.info(msg);
            }
        }
    }
});


function updateStreamInfo() {
    console.log('update');
    if ($('#group_size_1').is(':checked') && $('#stream_size_1').is(':checked')) {
        $('#chrome_warning').removeClass('hidden');
    } else {
        $('#chrome_warning').addClass('hidden');
    }
}

function startLessonModal() {

    if ($('#stream_type').length > 0) {
        startLessonCustom($('#stream_type').val(), $('#player_type').val(), $('#stream_record').is(':checked'));
    } else {
        // if ($('.stream-size-item:checked').length == 0 || $('.group-size-item:checked').length == 0) {
        if ($('.stream-size-item:checked').length == 0) {
            alert('Kérjük adja meg a tanóra indításához szükséges adatokat!');
        } else {
            // let type = $('#group_size_1').is(':checked') && $('#stream_size_1').is(':checked') ? 1 : 2;
            let type = 1;
            let stream_count = $('#stream_size_1').is(':checked') ? 1 : 2;
            startLesson(type, stream_count);
        }
    }


    
}

function confirmLessonDelete(id) {
    if (confirm('Biztosan törölni kívánja a tanórát?')) {
        location.href = '/lessons/' + id + '/delete';
    }
}

function confirmCourseDelete(id) {
    if (confirm('Biztosan törölni kívánja a kurzust és vele a teljes tartalmát (órák, csatolmányok és egyéb tartalmak)?')) {
        location.href = '/courses/' + id + '/delete';
    }
}