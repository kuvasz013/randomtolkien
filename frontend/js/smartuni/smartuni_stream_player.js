
let suStreamPlayer = null;

const SU_STATE = {
    READY: 1,
    PLAYING: 2,
    PAUSED: 3,
    STOPPED: 4,
    ERROR: 5,
    OTHER: 6
}

function SUStreamPlayer(options) {

    let dvr = options.dvr,
        useCdn = true,
        ios = options.ios || false,
        iPhone = options.iPhone || false,
        stream_id = options.stream_id,
        host = useCdn ? 'stream-cdn.smartuni.xyz' : '5de63c9a7c72b.streamlock.net',
        wowzaLicense = 'PLAY2-n6QKN-MfaXy-B7dvK-wJGff-KAN4B',
        manifest_suffix = 'playlist.m3u8' + (dvr ? '?DVR' : ''),
        updateInterval = null,
        logInterval = null,
        pBtn = $('#player_play_btn'),
        _this = this,
        logSet = new Set(),
        player_type = options.player_type || 'videojs',
        savedVolume = null,
        stream_type = options.stream_type || 'simple';

    var pipMode = 1,
        pipPosition = 1,
        layoutMode = 'parallel';


    var presentation_player = null;
    var teacher_player = null;

    if (player_type == 'videojs') {
        teacher_player = new VideojsPlayer({
            container_id: 'teacher_stream',
            manifest: `https://${host}/webrtc${useCdn?'_origin':''}/ngrp:${options.teacher_stream_id}_all/${manifest_suffix}`
        });
        teacher_player.initPlayer();

        if (stream_type == 'dual') {
            presentation_player = new VideojsPlayer({
                container_id: 'presentation_stream',
                manifest: `https://${host}/pres${useCdn?'_origin':''}/ngrp:${options.presentation_stream_id}_all/${manifest_suffix}`
            });
            presentation_player.initPlayer();
        }
    } else {

        teacher_player = new WowzaPlayer({
            container_id: 'teacher_stream',
            manifest: `https://${host}/webrtc${useCdn?'_origin':''}/ngrp:${options.teacher_stream_id}_all/${manifest_suffix}`,
            wowzaLicense: wowzaLicense
        });
        teacher_player.initPlayer();

        presentation_player = new WowzaPlayer({
            container_id: 'presentation_stream',
            manifest: `https://${host}/pres${useCdn?'_origin':''}/ngrp:${options.presentation_stream_id}_all/${manifest_suffix}`,
            wowzaLicense: wowzaLicense
        });
        presentation_player.initPlayer();

    }

    teacher_player.getContainer().click(function() {
        pipMode = 1;
    });

    teacher_player.getContainer().on('touchend', function() {
        pipMode = 1;
    });

    if (presentation_player) {
        presentation_player.getContainer().click(function() {
            pipMode = 2;
        });

        presentation_player.getContainer().on('touchend', function() {
            pipMode = 2;
        });
    }


    $(window).resize(function() {
        _this.updateLayout();
    });
    
    this.changeFront = function(mode) {

        pipMode = mode;
        pBtn.attr('disabled', 'disabled').addClass('disabled');
        setTimeout(function() {
            pBtn.removeAttr('disabled').removeClass('disabled');
            _this.play(true);
        }, 800);

        if (mode == 1) {
            $('#front_changer_1 i').addClass('text-primary');
            $('#front_changer_2 i').removeClass('text-primary');
        } else {
            $('#front_changer_1 i').removeClass('text-primary');
            $('#front_changer_2 i').addClass('text-primary');
        }
        
    }

    this.updateLayout = function() {

        let teacherSize = teacher_player.getVideoSize(),
        presSize = presentation_player ? presentation_player.getVideoSize() : null,
        teacherContainer = teacher_player.getContainer(),
        presContainer = presentation_player ? presentation_player.getContainer() : null;
        
        let playerContainer = $('#su_stream_player');

        if (stream_type == 'dual') {    
            if (iPhone) {
                layoutMode = 'iphone';
                layoutIphone({ teacherSize, presSize, teacherContainer, presContainer, playerContainer })
            } else {
                if (playerContainer.width() > 1000) {
                    layoutMode = 'parallel';
                    layoutParallel({ teacherSize, presSize, teacherContainer, presContainer, playerContainer });
                } else {
                    layoutMode = 'pip';
                    layoutPIP({ teacherSize, presSize, teacherContainer, presContainer, playerContainer });
                }
                $('#pip_container').hide();
            }
            
        } else {
            layoutMode = 'simple';
            layoutSimple({teacherSize, teacherContainer, playerContainer});
            $('#pip_container').hide();
        }

    }

    function layoutIphone(params) {

        $('#pip_container').show();
        
        let v1 = {
            size: pipMode == 1 ? params.teacherSize : params.presSize,
            container: pipMode == 1 ? params.teacherContainer : params.presContainer
        };

        let v2 = {
            size: pipMode == 2 ? params.teacherSize : params.presSize,
            container: pipMode == 2 ? params.teacherContainer : params.presContainer
        };

        v1.container.show();
        v2.container.hide();

        v1.container.css('zIndex', '110');
        v2.container.css('zIndex', '100');

        let ratio = v1.size.height / v1.size.width;
        let mainHeight = params.playerContainer.width() * ratio;
        params.playerContainer.height(mainHeight + 'px');
        v1.container.width('100%');
        v1.container.height(mainHeight+'px');

    }

    function layoutSimple(params) {
        $('#presentation_stream').hide();

        let ratio = params.teacherSize.height / params.teacherSize.width;
        let mainHeight = params.playerContainer.width() * ratio;
        params.playerContainer.height(mainHeight + 'px');
        params.teacherContainer.width('100%');
        params.teacherContainer.height(mainHeight+'px');

    }

    function layoutPIP(params) {

        let v1 = {
            size: pipMode == 1 ? params.teacherSize : params.presSize,
            container: pipMode == 1 ? params.teacherContainer : params.presContainer
        };

        let v2 = {
            size: pipMode == 2 ? params.teacherSize : params.presSize,
            container: pipMode == 2 ? params.teacherContainer : params.presContainer
        };

        let ratio1 = v1.size.height / v1.size.width,
            ratio2 = v2.size.height / v2.size.width;

        let mainHeight = params.playerContainer.width() * ratio1;

        params.playerContainer.height(mainHeight + 'px');

        
        let v1Width = Math.round(mainHeight / ratio1),
            v1Left = Math.round((params.playerContainer.width() - v1Width) / 2);

        v1.container.width(v1Width + 'px');
        v1.container.height(mainHeight + 'px');
        v1.container.css({left: v1Left, top: 0});

        let v2Height = mainHeight * 0.4,
            v2Width = v2Height / ratio2;

        let v2Left = v1Left + v1Width - v2Width,
            v2Top = pipPosition == 1 ? 0 : (mainHeight - v2Height);

        v2.container.width(v2Width + 'px');
        v2.container.height(v2Height + 'px');
        v2.container.css({left: v2Left, top: v2Top});

        v1.container.css('zIndex', '100');
        v2.container.css('zIndex', '110');

    }

    function layoutParallel(params) {

        // t 320 x 240  0.75
        // p 416 x 240  0.57

        // a 736 x 240  0.326

        // $('#su_player').height(mainHeight + 'px');

        let tRatio = params.teacherSize.height / params.teacherSize.width,
            pRatio = params.presSize.height / params.presSize.width;

        let refHeight = 400;
        let fullRatio = refHeight / (refHeight / tRatio + refHeight / pRatio);
        let mainHeight = params.playerContainer.width() * fullRatio;

        params.playerContainer.height(mainHeight + 'px');

        // console.log(params);

        let tHeight = mainHeight,
            tWidth = mainHeight / tRatio;

        let pHeight = mainHeight,
            pWidth = mainHeight / pRatio;

        // console.log({tWidth, pWidth, pRatio, tRatio});


        let tLeft = Math.round((params.playerContainer.width() - (tWidth + pWidth)) / 2),
            pLeft = tLeft + tWidth;

        let tTop = 0,
            pTop = 0;

        // console.log({ tLeft, tTop, pLeft, pTop });

        params.teacherContainer.height(tHeight + 'px');
        params.teacherContainer.width(tWidth + 'px');
        params.teacherContainer.css({top: tTop, left: tLeft});

        params.presContainer.height(pHeight + 'px');
        params.presContainer.width(pWidth + 'px');
        params.presContainer.css({top: pTop, left: pLeft});

    }

    teacher_player.setVolume(80);

    this.tplayer = teacher_player;
    this.pplayer = presentation_player;

    this.toggleMute = function(btn) {
        if (_this.tplayer.getVolume() == 0) {
            let targetVolume = savedVolume != null ? savedVolume : 80;
            _this.setVolume(targetVolume);
            savedVolume = null;
        } else {
            savedVolume = _this.tplayer.getVolume();
            _this.setVolume(0);
        }
        
        updateControls();
    }

    this.play = function (force) {

        if (stream_type == 'simple') {
            playSimple(force);
        } else {
            if (iPhone) {
                playIphone(force);
            } else {
                playDual(force);
            }
            
        }   

        if (!$('#init_btn').hasClass('hidden')) {
            $('#init_btn').addClass('hidden');
            $('#su_player_footer').removeClass('hidden');
        }

        startUIUpdate();
        // startLogging();
        updateControls();
    }

    function playIphone(force) {
        let player_front = getPlayerFront(),
            player_back = getPlayerBack();

        if (!force && player_front.getState() == SU_STATE.PLAYING) {
            player_front.pause();
        } else {
            player_front.play();
        }

        // player_back.pause();

    }

    function playSimple(force) {
        if (!force && _this.tplayer.getState() === SU_STATE.PLAYING) {
            _this.tplayer.pause();
        } else {
            _this.tplayer.play();
        }
    }

    function playDual(force) {
        if (!force && (_this.tplayer.getState() === SU_STATE.PLAYING || _this.pplayer.getState() === SU_STATE.PLAYING)) {
            _this.tplayer.pause();
            _this.pplayer.pause();
        } else {
            _this.tplayer.play();
            _this.pplayer.play();
        }
    }

    this.finish = function () {
        teacher_player.finish();
        if (presentation_player) {
            presentation_player.finish();
        }
    }

    this.changeVolume = function (e) {
        let boxLeft = $(e.target).offset().left;
        let boxWidth = 50;
        let percent = Math.round((e.pageX - boxLeft) / 0.5);
        _this.setVolume(percent);
    }

    function getPlayerFront() {
        if (layoutMode == 'simple' || layoutMode == 'parallel') {
            return _this.tplayer;
        } else if (layoutMode == 'pip' || layoutMode == 'iphone') {
            return pipMode == 1 ? _this.tplayer : _this.pplayer;
        }
    }

    function getPlayerBack() {
        if (layoutMode == 'simple' || layoutMode == 'parallel') {
            return null;
        } else if (layoutMode == 'pip' || layoutMode == 'iphone') {
            return pipMode == 1 ? _this.pplayer : _this.tplayer;
        }
    }

    this.setVolume = function(volume) {

        let player_front = getPlayerFront(),
            player_back = getPlayerBack();

        $('#player_volume .progress-bar').css({ width: volume + '%' });
        player_front.setVolume(volume);
        if (player_back) {
            player_back.setVolume(0);
        }
        
        if (volume == 0) {
            $('#volume_icon').removeClass('fa-volume-up fa-volume-down').addClass('fa-volume-mute');
        } else if (volume < 50) {
            $('#volume_icon').removeClass('fa-volume-mute fa-volume-up').addClass('fa-volume-down');
        } else {
            $('#volume_icon').removeClass('fa-volume-mute fa-volume-down').addClass('fa-volume-up');
        }
    }

    this.getCurrentTime = function () {
        return Math.round(_this.tplayer.getCurrentTime() / 1000);
    }

    function onPlayheadTime(e) {
        // console.log('onPlayHeadTime ', e);
        handlePlayLog(Math.round(e.time / 1000));
    }

    function handlePlayLog(time) {
        logSet.add(time);
    }

    function doLog() {

        if (logSet.size > 0) {
            axios.post('/stream/log', {
                stream_id,
                times: [...logSet].join(',')
            }).then(function (resp) {
                if (resp.data.success) {
                    logSet.clear();
                }
            }).catch(function (err) {
                console.error(err);
            });
        }
    }

    function updateControls() {

        if (stream_type == 'simple') {
            if (_this.tplayer.getState() === SU_STATE.PLAYING) {
                pBtn.find('i').removeClass('fa-play');
                pBtn.find('i').addClass('fa-pause');
            } else {
                pBtn.find('i').addClass('fa-play');
                pBtn.find('i').removeClass('fa-pause');
            }
        } else {
            if (layoutMode == 'iphone') {
                let player_front = getPlayerFront();
                if (player_front.getState() == SU_STATE.PLAYING) {
                    pBtn.find('i').removeClass('fa-play');
                    pBtn.find('i').addClass('fa-pause');
                } else {
                    pBtn.find('i').addClass('fa-play');
                    pBtn.find('i').removeClass('fa-pause');
                }
                $('#player_volume_container').hide();
            } else {
                if (_this.tplayer.getState() === SU_STATE.PLAYING || _this.pplayer.getState() === SU_STATE.PLAYING) {
                    pBtn.find('i').removeClass('fa-play');
                    pBtn.find('i').addClass('fa-pause');
                } else {
                    pBtn.find('i').addClass('fa-play');
                    pBtn.find('i').removeClass('fa-pause');
                }
            }

            
        }
        

    }

    function startLogging() {
        if (logInterval === null) {
            logInterval = setInterval(doLog, 10000);
        }

    }

    function startUIUpdate() {
        if (updateInterval === null) {
            updateInterval = setInterval(updateUI, 500);
        }
    }

    function updateUI() {
        $('#player_time_current').html(moment.unix(_this.tplayer.getTime()).format('mm:ss'));
        _this.updateLayout();
    }

}

function WowzaPlayer(options) {

    const WOWZA_STATE = {
        UNKNOWN: 0,
        CREATED: 1,
        INITIALIZED: 2,
        PREPARING: 3,
        READY_TO_PLAY: 4,
        PLAYING: 5,
        PAUSED: 6,
        STOPPED: 7,
        PLAYBACK_COMPLETE: 8,
        SHUTDOWN: 9,
        APP_STOPPED: 10,
        ERROR: 11,
    };

    const player_config = {
        "license": options.wowzaLicense,
        "sourceURL": options.manifest,
        "uiShowPlaybackControls": options.ios || false,
        "uiShowFullscreen": options.ios || false,
        "uiShowQuickRewind": options.ios || false,
        "uiShowVolumeControl": options.ios || false,
        "uiShowSeekBar": options.ios || false,
        "startAtLivePoint": true,
        "stringLiveLabel": "ÉLŐ",
        "uiShowBitrateSelector": true
    };

    var player = null,
        video = null;

    function insertTemplate(container_id) {
        video = $('<div></div');
        video.attr('id', 'player_'+options.container_id);
        video.css({width: '100%', height: '440px'});
        $('#'+container_id).append(video);
    }

    this.initPlayer = function() {
        insertTemplate(options.container_id);
        player = WowzaPlayer.create(video.attr('id'), player_config);
        addListeners();
    }

    this.getTime = function(ms) {
        return ms ? player.getCurrentTime() : Math.round(player.getCurrentTime() / 1000);
    }

    this.getState = function() {
        let state = player.getCurrentState();
        
        if (state == WOWZA_STATE.READY_TO_PLAY) {
            return SU_STATE.READY;
        } else if (state == WOWZA_STATE.PLAYING) {
            return SU_STATE.PLAYING;
        } else if (state == WOWZA_STATE.PAUSED) {
            return SU_STATE.PAUSED;
        } else if (state == WOWZA_STATE.STOPPED) {
            return SU_STATE.STOPPED;
        } else if (state == WOWZA_STATE.ERROR) {
            return SU_STATE.ERROR;
        } else {
            return SU_STATE.OTHER;
        }

    }

    this.play = function() {
        player.play();
    }

    this.pause = function() {
        player.pause();
    }

    this.setVolume = function(volume) {
        player.setVolume(volume);
    }

    this.mute = function() {
        player.setVolume(0);
    }

    function addListeners() {
        player.onCompleted(function ( completedEvent ) {
            console.log('SUPlayer ('+options.container_id+') onCompleted : Time: ' + completedEvent.time);
        });

        player.onError(function ( errorEvent ) {
            console.log('SUPlayer ('+options.container_id+') onError : ' + errorEvent.error);
        });

        player.onPause(function ( pauseEvent ) {
            console.log('SUPlayer ('+options.container_id+') onPause : ' + pauseEvent.time);
        });

        player.onPlaybackAttempt(function ( playbackEvent ) {
            console.log('SUPlayer ('+options.container_id+') onPlaybackAttempt : ' + playbackEvent.url);
        });

        player.onPlaybackFailure(function ( playbackEvent ) {
            console.log('SUPlayer ('+options.container_id+') onPlaybackFailure : ' + playbackEvent.url);
        });

        player.onStateChanged(function ( stateChangedEvent ) {
            console.log('SUPlayer ('+options.container_id+') onStateChanged : ' + stateChangedEvent.currentState);
        });

        player.onStop(function ( stopEvent ) {
            console.log('SUPlayer ('+options.container_id+') onStop : ' + stopEvent.time);
        });
    }
}

function VideojsPlayer(options) {


    var player = null,
        video = null,
        container = null;

    function insertTemplate(container_id) {
        container = $('#'+container_id);
        video = $('<video playsinline></video');
        video.addClass('video-js vjs-default-skin vjs-layout-medium');
        video.attr('id', 'player_'+options.container_id);
        video.css({width: '100%', height: '100%'});
        container.append(video);
    }

    this.getContainer = function() {
        return container;
    }

    this.getTech = function() {
        return player;
    }

    this.initPlayer = function() {

        insertTemplate(options.container_id);

        player = videojs(video.attr('id'), {liveui: true});
        player.src({
            src: options.manifest,
            type: 'application/x-mpegURL',
            overrideNative: true,
            fill: true
        });
        player.hlsQualitySelector({
            displayCurrentQuality: true
        });
        player.reloadSourceOnError();
    }
    
    
    this.getTime = function() {
        return player.currentTime();
    }

    this.getState = function() {
        if (player.paused()) {
            return SU_STATE.PAUSED;
        } else {
            return SU_STATE.PLAYING;
        }
        
    }

    this.play = function() {
        player.play();
        // player.liveTracker.seekToLiveEdge();
    }

    this.pause = function() {
        player.pause();
    }

    this.getVolume = function() {
        return Math.round(player.volume() * 100);
    }

    this.setVolume = function(volume) {
        player.volume(volume/100);
    }

    this.mute = function() {
        player.volume(0);
    }

    this.getVideoSize = function() {
        return {
            width: player.videoWidth(),
            height: player.videoHeight()
        }
    }
}