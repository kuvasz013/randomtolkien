function initPoll() {
    // Initialize multiple switch
    let multipleId = 'mu_' + poll.id;
    $('.switchery').attr('id', multipleId).attr('name', multipleId);
    $('#label-multiple').attr('for', multipleId);

    // Load state of multiple switch from poll data
    if (poll.multiple) $('.switchery').attr('checked', poll.multiple);

    $('.switchery').each(function (idx, obj) {
        new Switchery($(this)[0], $(this).data());
    });


    // Initialize poll answers
    if (poll.answers) {
        poll.answers.forEach(answer => {
            addPollAnswer(null, answer);
        });
    }
}


function createPoll() {

    let poll = serializePoll();
    console.log(poll);

    axios.post('/polls/create', {
        poll
    }).then(function (resp) {
        if (resp) {
            location.href = '/polls/'+resp.data.id+'/edit';
            console.log(resp.data);
        }
    }).catch(function (err) {
        console.error(err);
        Notification.error('Hiba a mentés közben!');
    });
}



function savePoll() {

    let poll = serializePoll();
    console.log(poll);

    axios.post('/polls/' + poll.id + '/update', {
        poll
    }).then(function (resp) {
        if (resp) {
            Notification.success('A szavazást elmentettük!')
        }
    }).catch(function (err) {
        console.error(err);
        Notification.error('Hiba a mentés közben!');
    });
}


function serializePoll() {
    let poll = {
        id: $('#poll-id').val() || null,
        course_id: $('#course_id').val() || null,
        lesson_id: $('#lesson_id').val() || null,
        title: $('#poll-title').val(),
        description: $('#poll-description').val(),
        multiple: $('.switchery').is(':checked') || false,
        answers: []
    };

    $('#poll-answers .poll-answer').each(function () {
        let a = $(this);
        let answer = {
            id: a.attr('data-id') || null,
            answer: a.find('input.poll-answer-text').val()
        };
        poll.answers.push(answer);
    });

    return poll;
}


function addPollAnswer(btn, data) {
    let template = $('#poll-templates').find('.poll-answer').clone();

    if (data) {
        template.attr('data-id', data.id);
        template.find('input').val(data.answer);
    }

    $('#poll-answers').append(template);
}


function submitPoll() {
    let submit = serializePollSubmit();

    axios.post('/polls/' + poll.poll_id + '/submit', {
        submit

    }).then(function (resp) {
        if (resp) {
            console.log(resp.data);
            $('#submit-controls, #poll').addClass('hidden');
            $('#submit-thankyou, #submit-back').removeClass('hidden');
        }

    }).catch(function (err) {
        console.error(err);
    });
}


function serializePollSubmit() {

    let submit = {
        poll_id: $('#poll-header').attr('data-id'),
        lesson_id: lesson_id,
        answers: []
    };

    $('#poll').find('input:checked').each(function () {
        submit.answers.push($(this).attr('data-id'));
    });

    return submit;

}


function removePollAnswer(btn) {
    $(btn).closest('.poll-answer').remove();
}


if (typeof(poll) != 'undefined') {
    $(initPoll);
}
