




function WebRTCPlayer(options) {
	
	var peerConnection = null;
	var peerConnectionConfig = {'iceServers': []};
	var localStream = null;
	var localVideo = null;
	var wsURL = "wss://5de63c9a7c72b.streamlock.net/webrtc-session.json";
	var wsConnection = null;
	var streamInfo = {
		applicationName: options.applicationName,
		streamName: options.streamName,
		sessionId:"[empty]"
	};
	var newAPI = false;
	var repeaterRetryCount = 0;
	
	localVideo = document.getElementById(options.localVideoId);
	
	
	function wsConnect(url)
	{
		wsConnection = new WebSocket(url);
		wsConnection.binaryType = 'arraybuffer';
		
		wsConnection.onopen = function()
		{
			console.log("wsConnection.onopen");
			
			peerConnection = new RTCPeerConnection(peerConnectionConfig);
			peerConnection.onicecandidate = gotIceCandidate;
			
			if (newAPI)
			{
				peerConnection.ontrack = gotRemoteTrack;
			}
			else
			{
				peerConnection.onaddstream = gotRemoteStream;
			}
	
			console.log("wsURL: "+wsURL);
			sendPlayGetOffer();
		}
		
		function sendPlayGetOffer()
		{
			console.log("sendPlayGetOffer: "+JSON.stringify(streamInfo));
			wsConnection.send('{"direction":"play", "command":"getOffer", "streamInfo":'+JSON.stringify(streamInfo)+'}');
		}
	
		wsConnection.onmessage = function(evt)
		{
			console.log("wsConnection.onmessage: "+evt.data);
			
			var msgJSON = JSON.parse(evt.data);
			
			var msgStatus = Number(msgJSON['status']);
			var msgCommand = msgJSON['command'];
			
			if (msgStatus == 514) // repeater stream not ready
			{
				repeaterRetryCount++;
				if (repeaterRetryCount < 10)
				{
					setTimeout(sendGetOffer, 500);
				}
				else
				{
					$("#sdpDataTag").html('Live stream repeater timeout: '+streamName);
					stopPlay();
				}
			}
			else if (msgStatus != 200)
			{
				$("#sdpDataTag").html(msgJSON['statusDescription']);
				stopPlay();
			}
			else
			{
				$("#sdpDataTag").html("");
	
				var streamInfoResponse = msgJSON['streamInfo'];
				if (streamInfoResponse !== undefined)
				{
					streamInfo.sessionId = streamInfoResponse.sessionId;
				}
	
				var sdpData = msgJSON['sdp'];
				if (sdpData !== undefined)
				{
					console.log('sdp: '+JSON.stringify(msgJSON['sdp']));
	
					peerConnection.setRemoteDescription(new RTCSessionDescription(msgJSON.sdp), function() {
						peerConnection.createAnswer(gotDescription, errorHandler);
					}, errorHandler);
				}
	
				var iceCandidates = msgJSON['iceCandidates'];
				if (iceCandidates !== undefined)
				{
					for(var index in iceCandidates)
					{
						console.log('iceCandidates: '+JSON.stringify(iceCandidates[index]));
						peerConnection.addIceCandidate(new RTCIceCandidate(iceCandidates[index]));
					}
				}
			}
			
			if ('sendResponse'.localeCompare(msgCommand) == 0)
			{
				if (wsConnection != null)
					wsConnection.close();
				wsConnection = null;
			}
			// now check for getAvailableResponse command to close the connection 
			if ('getAvailableStreams'.localeCompare(msgCommand) == 0)
			{
				stopPlay();
			}
		}
		
		wsConnection.onclose = function()
		{
			console.log("wsConnection.onclose");
		}
		
		wsConnection.onerror = function(evt)
		{
			console.log("wsConnection.onerror: "+JSON.stringify(evt));
			
			$("#sdpDataTag").html('WebSocket connection failed: '+wsURL);
		}
	}
	

	this.startPlay = function()
	{
		if (localStream != null) {
			repeaterRetryCount = 0;				
			console.log("startPlay: wsURL:"+wsURL+" streamInfo:"+JSON.stringify(streamInfo));
			wsConnect(wsURL);
		}
		
	}
	
	this.stopPlay = function()
	{
		if (peerConnection != null)
			peerConnection.close();
		peerConnection = null;
		
		if (wsConnection != null)
			wsConnection.close();
		wsConnection = null;
		
		localVideo.src = ""; // this seems like a chrome bug - if set to null it will make HTTP request
	
		console.log("stopPlay");
	
	}
	
	// start button clicked
	function start() 
	{
	
		if (peerConnection == null)
			startPlay();
		else
			stopPlay();
	}
	
	function gotMessageFromServer(message) 
	{
		var signal = JSON.parse(message.data);
		if(signal.sdp) 
		{
			if (signal.sdp.type == 'offer')
			{
				console.log('sdp:offser');
				console.log(signal.sdp.sdp);
				peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), function() {
					peerConnection.createAnswer(gotDescription, errorHandler);
				}, errorHandler);
			}
			else
			{
				console.log('sdp:not-offer: '+signal.sdp.type);
			}
	
		}
		else if(signal.ice)
		{
			console.log('ice: '+JSON.stringify(signal.ice));
			peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice));
		}
	}
	






	
	function gotIceCandidate(event) 
	{
		if(event.candidate != null) 
		{
		}
	}
	
	function gotDescription(description) 
	{
		console.log('gotDescription');
		peerConnection.setLocalDescription(description, function () 
		{
			console.log('sendAnswer');
	
			wsConnection.send('{"direction":"play", "command":"sendResponse", "streamInfo":'+JSON.stringify(streamInfo)+', "sdp":'+JSON.stringify(description)+'}');
	
		}, function() {console.log('set description error')});
	}
	
	function gotRemoteTrack(event) 
	{
		console.log('gotRemoteTrack: kind:'+event.track.kind+' stream:'+event.streams[0]);
		try{
				localVideo.srcObject = event.streams[0];
				localStream = event.streams[0];
		} catch (error){
				localVideo.src = window.URL.createObjectURL(event.streams[0]);
		}
	}
	
	function gotRemoteStream(event) 
	{
		console.log('gotRemoteStream: '+event.stream);
		try{
			localVideo.srcObject = event.stream;
			localStream = event.stream;
		} catch (error){
			localVideo.src = window.URL.createObjectURL(event.stream);
		}
	}
	
	function errorHandler(error) 
	{
		console.log(error);
	}

}




function SmartUniPlayer(options) {

	var teacherRtc = new WebRTCPlayer({
		localVideoId: 'teacherVideo',
		applicationName: 'webrtc',
		streamName: options.teacher_stream_id
	});

	var presentationRtc = new WebRTCPlayer({
		localVideoId: 'presentationVideo',
		applicationName: 'pres',
		streamName: options.presentation_stream_id,
		presentation: true
	});

	this.start = function() {
		teacherRtc.startPlay();
		presentationRtc.startPlay();
	}

	this.stop = function() {
		teacherRtc.stopPlay();
		presentationRtc.stopPlay();
	}
	

}

function SmartUniHlsPlayer(options) {

	

	var teacher_player_hls = initPlayer({
		player_id: 'teacherVideo',
		manifest: `https://5de63c9a7c72b.streamlock.net/webrtc/${options.teacher_stream_id}/playlist.m3u8?DVR`
	});
	var presentation_player_hls = initPlayer({
		player_id: 'presentationVideo',
		manifest: `https://5de63c9a7c72b.streamlock.net/pres/${options.presentation_stream_id}/playlist.m3u8?DVR`
	});

	function initPlayer(options) {
		let player = videojs(options.player_id);
		player.src({
			src: options.manifest,
			type: 'application/x-mpegURL',
			overrideNative: true,
			fill: true
		});
		player.hlsQualitySelector({
			displayCurrentQuality: true
		});
		player.dvr();

		return player;
	}









	this.tplayer = teacher_player_hls;
	this.pplayer = presentation_player_hls;






	this.start = function() {
		teacher_player_hls.play();
		presentation_player_hls.play();
	}

}


function SmartUniWPlayer(options) {

	let dvr = true,
		manifest_suffix = 'playlist.m3u8' + (dvr ? '?DVR' : '');

	var teacher_player = initPlayer({
		player_id: 'teacherVideo',
		manifest: `https://5de63c9a7c72b.streamlock.net/webrtc/${options.teacher_stream_id}_source/${manifest_suffix}`
	});
	var presentation_player = initPlayer({
		player_id: 'presentationVideo',
		manifest: `https://5de63c9a7c72b.streamlock.net/pres/${options.presentation_stream_id}/${manifest_suffix}`
	});

	function initPlayer(options) {
		let player = WowzaPlayer.create(options.player_id, {
			"license":"PLAY2-n6QKN-MfaXy-B7dvK-wJGff-KAN4B",
			"sourceURL": options.manifest,
			"uiShowPlaybackControls": false,
			"uiShowFullscreen": false,
			"uiShowQuickRewind": false,
			"uiShowVolumeControl": false,
			"uiShowSeekBar": false,
			"startAtLivePoint": true,
			"stringLiveLabel": "ÉLŐ",
			"uiShowBitrateSelector": true
			}
		);

		player.onMetadata(function(metaDataEvent) {
			console.log("meta: " + metaDataEvent.packetTime + " , " + metaDataEvent.time + " , " + metaDataEvent.type);
		});

		return player;
	}

	this.tplayer = teacher_player;
	this.pplayer = presentation_player;

	this.play = function() {
		teacher_player.play();
		presentation_player.play();
	}

	this.pause = function() {
		teacher_player.pause();
		presentation_player.pause();
	}

	this.finish = function() {
		teacher_player.finish();
		presentation_player.finish();
	}

}



var	smartUniPlayer = null,
	smartUniHlsPlayer = null,
	smartUniWPlayer = null;



function initSmartUniPlayer(teacher_stream_id, presentation_stream_id) {

	smartUniPlayer = new SmartUniPlayer({
		teacher_stream_id,
		presentation_stream_id
	});

}

function initSmartUniHlsPlayer(teacher_stream_id, presentation_stream_id) {

	smartUniHlsPlayer = new SmartUniHlsPlayer({
		teacher_stream_id,
		presentation_stream_id
	});

}

function initSmartUniWPlayer(teacher_stream_id, presentation_stream_id) {

	smartUniWPlayer = new SmartUniWPlayer({
		teacher_stream_id,
		presentation_stream_id
	});

}

function setStreamStatus(course_id, stream_id, status) {

	axios.post('/courses/' + course_id + '/streamstatus', {
			id: stream_id,
			status: status
		})
		.then(function (response) {
			// handle success
			console.log(response.data);
		})
		.catch(function (error) {
			// handle error
			console.log(error);
		});

}