function WebRTCStreamer(options) {


	var localVideo = null;
	var localVideoId = options.localVideoId;
	var presentation = options.presentation || false;
	var remoteVideo = null;
	var peerConnection = null;
	var peerConnectionConfig = {'iceServers': []};
	var localStream = null;
	var wsURL = options.wsURL || "wss://5de63c9a7c72b.streamlock.net/webrtc-session.json";
	var wsConnection = null;
	var userData = {param1:"value1"};
	var videoBitrate = options.videoBitrate || (presentation ? 400 : 1000);
	var audioBitrate = options.audioBitrate || 128;
	var videoFrameRate = options.videoFrameRate || (presentation ? 12 : null);
	var videoChoice = "42e01f";
	var audioChoice = "opus";
	var videoIndex = -1;
	var audioIndex = -1;
	var userAgent = null;
	var newAPI = false;
	var SDPOutput = new Object();
	var streamInfo = {
		applicationName: options.applicationName,
		streamName: options.streamName,
		sessionId:"[empty]"
	};
	var hasAudio = false;
	var _this = this;
	var sourceChooserBtn = options.sourceChooserBtn || 'source_chooser_btn';
	var stateChangeCallback = options.stateChangeCallback || null;

	navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
	window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
	window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
	window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

	localVideo = document.getElementById(localVideoId);

	// $('#'+sourceChooserBtn).click(bindToVideo);
	// document.getElementById(sourceChooserBtn).addEventListener('click', bindToVideo);
	// localVideo.addEventListener('click', bindToVideo);

	var source_chooser_btn = null;

	function bindToVideo(is_presentation) {
		if (localStream == null) {

			if (typeof(is_presentation) != 'undefined' && is_presentation != null) {
				presentation = is_presentation;
			}

			if (presentation) {
				_this.switchDesktopCapture();
			} else {
				_this.bindStream();
			}
		}
	}

	this.bind = function(is_presentation, btn) {
		console.log('Bind ' + is_presentation + ' -- ' , btn);
		source_chooser_btn = $(btn);
		bindToVideo(is_presentation);
	}

	this.getStatus = function() {
		if (peerConnection) {
			return 'streaming';
		} else if (localStream) {
			return 'binded';
		} else {
			return 'ready';
		}
	}

	function updateControls() {
		console.log('Update controls');
		if (localStream) {
			// console.log('Hide btn: ' + sourceChooserBtn);
			// $('#'+sourceChooserBtn).addClass('hidden');
			// $('#su_stream_controls').hide();
			console.log(source_chooser_btn);
			if (source_chooser_btn.hasClass('multi-source-item')) {
				console.log('Hide all buttons');
				$('#su_stream_controls').hide();
			} else {
				console.log('Hide single button: ', source_chooser_btn);
				$(source_chooser_btn).hide();
			}
			
		} else {
			// console.log('Show btn: ' + sourceChooserBtn);
			// $('#'+sourceChooserBtn).removeClass('hidden');
			// $('#su_stream_controls').show();
			if (source_chooser_btn.hasClass('multi-source-item')) {
				$('#su_stream_controls').show();
			} else {
				$(source_chooser_btn).show();
			}
		}
	}

	function pageReady()
	{

		/*
			// firefox
			video: {
				width: { min: 1280, ideal: 1280, max: 1920 },
				height: { min: 720, ideal: 720, max: 1080 }
			},

			// chrome
			video: {
				mandatory: {
					minWidth: 1280,
					maxWidth: 1280,
					minHeight: 720,
					maxHeight: 720,
					minFrameRate: 30,
					maxFrameRate: 30
				}
			},

			video: {
				mandatory: {
					minAspectRatio: 1.7777777778
				}
			},

			video: true,
		*/

        

	
    }
    
    this.bindStream = function(sourceId, addAudioTrack) {
        var constraints = {};
        if (sourceId) {
            constraints = {
                audio: false, // ha true, akkor Mac-en AbortError jön (ok az oldal alján: https://electronjs.org/docs/api/desktop-capturer )
                video: {
                    mandatory: {
                        chromeMediaSource: 'desktop',
                        chromeMediaSourceId: sourceId
                    },
                    optional: []
                }
        };
        }  else {
            constraints = {
                video: true,
                audio: true
            };
        }
		

		if(navigator.mediaDevices.getUserMedia)
		{
			navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {

				if (addAudioTrack) {
					navigator.mediaDevices.getUserMedia({video: false, audio:true}).then(function(audioStream) {

						stream.addTrack(audioStream.getAudioTracks()[0]);
						getUserMediaSuccess(stream);

					});
				} else {
					getUserMediaSuccess(stream);
				}

				
			}).catch(errorHandler);
			newAPI = false;
		} else {
			console.log('No mediaDevices');
		}

		updateControls();

	}
	
	function onConnectionStateChange(event) {
		
		console.log("New state: " + peerConnection.connectionState);
		if (stateChangeCallback) {
			stateChangeCallback(peerConnection.connectionState);
		}
	}

	function wsConnect(url)
	{
		wsConnection = new WebSocket(url);
		wsConnection.binaryType = 'arraybuffer';

		wsConnection.onopen = function()
		{
			console.log("wsConnection.onopen");

			peerConnection = new RTCPeerConnection(peerConnectionConfig);
			peerConnection.onicecandidate = gotIceCandidate;
			peerConnection.onconnectionstatechange = onConnectionStateChange;

			if (newAPI)
			{
				var localTracks = localStream.getTracks();
				for(localTrack in localTracks)
				{
					peerConnection.addTrack(localTracks[localTrack], localStream);
				}
			}
			else
			{
				peerConnection.addStream(localStream);
			}

			peerConnection.createOffer(gotDescription, errorHandler);


		}

	//var offerOptions = {
		// New spec states offerToReceiveAudio/Video are of type long (due to
		// having to tell how many "m" lines to generate).
		// http://w3c.github.io/webrtc-pc/#idl-def-RTCOfferAnswerOptions.
	//  offerToReceiveAudio: 1,
	// offerToReceiveVideo: 1,
	//	codecPayloadType: 0x42E01F,
	// };

		wsConnection.onmessage = function(evt)
		{
			console.log("wsConnection.onmessage: "+evt.data);

			var msgJSON = JSON.parse(evt.data);

			var msgStatus = Number(msgJSON['status']);
			var msgCommand = msgJSON['command'];

			if (msgStatus != 200)
			{
				$("#sdpDataTag").html(msgJSON['statusDescription']);
				stopPublisher();
			}
			else
			{
				$("#sdpDataTag").html("");

				var sdpData = msgJSON['sdp'];
				if (sdpData !== undefined)
				{
					console.log('sdp: '+msgJSON['sdp']);

					peerConnection.setRemoteDescription(new RTCSessionDescription(sdpData), function() {
						//peerConnection.createAnswer(gotDescription, errorHandler);
					}, errorHandler);
				}

				var iceCandidates = msgJSON['iceCandidates'];
				if (iceCandidates !== undefined)
				{
					for(var index in iceCandidates)
					{
						console.log('iceCandidates: '+iceCandidates[index]);

						peerConnection.addIceCandidate(new RTCIceCandidate(iceCandidates[index]));
					}
				}
			}

			if (wsConnection != null)
				wsConnection.close();
			wsConnection = null;
		}

		wsConnection.onclose = function()
		{
			console.log("wsConnection.onclose");
		}

		wsConnection.onerror = function(evt)
		{
			console.log("wsConnection.onerror: "+JSON.stringify(evt));

			$("#sdpDataTag").html('WebSocket connection failed: '+wsURL);
			stopPublisher();
		}
	}

	function getUserMediaSuccess(stream)
	{

        console.log('Got stream');
        let audioTracks = stream.getAudioTracks();
		let videoTracks = stream.getVideoTracks();
		if (audioTracks[0]) {
			$('#track_audio').html(audioTracks[0].label);
			_this.hasAudio = true;
		} else {
			_this.hasAudio = false;
		}
		$('#track_video').html(videoTracks[0].label);
		let videoSettings = videoTracks[0].getSettings();
		videoFrameRate = Math.floor(videoSettings.frameRate);
		console.log(videoSettings);

        console.log('audio tracks: ', audioTracks);
        console.log('video tracks: ', videoTracks);

		console.log("getUserMediaSuccess: "+stream);
		localStream = stream;
		try{
			localVideo.srcObject = stream;
		} catch (error){
            console.log(error);
			localVideo.src = window.URL.createObjectURL(stream);
		}

		updateControls();
    }

    this.checkExtension = function() {
		var callback = function (message) {

			if (message.data == "smartedu-extension-loaded") {
                // thiz.callback("screen_share_extension_available");
                console.log('Got extension');
				window.removeEventListener("message", callback);
			}

		};
		//add event listener for desktop capture
		window.addEventListener("message", callback, false);

		window.postMessage("are-you-there", "*");

	};
    
    this.switchDesktopCapture = function() {

		var screenShareExtensionCallback =  function(message) {

			if (message.data == "smartedu-extension-loaded") {
				console.log("smartedu-extension-loaded parameter is received");
				// window.postMessage("get-sourceId", "*");
				window.postMessage("smartedu-audioAndTab", "*");
			}
			else if (message.data == "PermissionDeniedError") {
				console.debug("Permission denied error");
				// thiz.callbackError("screen_share_permission_denied");
			}
			else if (message.data && message.data.sourceId) {
                /*
				var mediaConstraints = {
						audio: false,
						video: {
							mandatory: {
								chromeMediaSource: 'desktop',
								chromeMediaSourceId: message.data.sourceId,
							},
							optional: []
						}
                };
                */
                
                console.log('Source Id: ', message.data.sourceId);

                _this.bindStream(message.data.sourceId, true);
                /*
				thiz.switchVideoSource(streamId, mediaConstraints, function(event) {
					thiz.callback("screen_share_stopped");
					thiz.switchVideoCapture(streamId);
                });
                */

				//remove event listener
				window.removeEventListener("message", screenShareExtensionCallback);
			}
		}


		//add event listener for desktop capture
		window.addEventListener("message", screenShareExtensionCallback, false);

        console.log('Are you there?');
        window.postMessage("are-you-there", "*");
        console.log('He?');
	}

	this.startPublisher = function()
	{
		if (peerConnection != null) {
			console.log('Peer already connected');
			return;
		}
		
		console.log("startPublisher: wsURL:"+wsURL+" streamInfo:"+JSON.stringify(streamInfo));

		wsConnect(wsURL);
	}

	this.stopPublisher = function()
	{
		if (peerConnection != null)
			peerConnection.close();
		peerConnection = null;

		if (wsConnection != null)
			wsConnection.close();
		wsConnection = null;

		console.log("stopPublisher");
	}


	function gotIceCandidate(event)
	{
		if(event.candidate != null)
		{
			console.log('gotIceCandidate: '+JSON.stringify({'ice': event.candidate}));
		}
	}

	function gotDescription(description)
	{
		var enhanceData = new Object();

		if (audioBitrate !== undefined)
			enhanceData.audioBitrate = Number(audioBitrate);
		if (videoBitrate !== undefined)
			enhanceData.videoBitrate = Number(videoBitrate);
		if (videoFrameRate !== undefined)
			enhanceData.videoFrameRate = Number(videoFrameRate);


		description.sdp = enhanceSDP(description.sdp, enhanceData);

		console.log('gotDescription: '+JSON.stringify({'sdp': description}));

		peerConnection.setLocalDescription(description, function () {

			wsConnection.send('{"direction":"publish", "command":"sendOffer", "streamInfo":'+JSON.stringify(streamInfo)+', "sdp":'+JSON.stringify(description)+', "userData":'+JSON.stringify(userData)+'}');

		}, function() {console.log('set description error')});
	}

	function addAudio(sdpStr, audioLine)
	{
		var sdpLines = sdpStr.split(/\r\n/);
		var sdpSection = 'header';
		var hitMID = false;
		var sdpStrRet = '';
		var done = false;

		for(var sdpIndex in sdpLines)
		{
			var sdpLine = sdpLines[sdpIndex];

			if (sdpLine.length <= 0)
				continue;


			sdpStrRet +=sdpLine;
			sdpStrRet += '\r\n';

			if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == false )
			{
				sdpStrRet +=audioLine;
				done = true;
			}


		}
		return sdpStrRet;
	}

	function addVideo(sdpStr, videoLine)
	{
		var sdpLines = sdpStr.split(/\r\n/);
		var sdpSection = 'header';
		var hitMID = false;
		var sdpStrRet = '';
		var done = false;

		var rtcpSize = false;
		var rtcpMux = false;

		for(var sdpIndex in sdpLines)
		{
			var sdpLine = sdpLines[sdpIndex];

			if (sdpLine.length <= 0)
				continue;

			if ( sdpLine.includes("a=rtcp-rsize") )
			{
				rtcpSize = true;
			}

			if ( sdpLine.includes("a=rtcp-mux") )
			{
				rtcpMux = true;
			}

		}

		for(var sdpIndex in sdpLines)
		{
			var sdpLine = sdpLines[sdpIndex];

			sdpStrRet +=sdpLine;
			sdpStrRet += '\r\n';

			if ( ('a=rtcp-rsize'.localeCompare(sdpLine) == 0 ) && done == false && rtcpSize == true)
			{
				sdpStrRet +=videoLine;
				done = true;
			}

			if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == true && rtcpSize == false)
			{
				sdpStrRet +=videoLine;
				done = true;
			}

			if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == false && rtcpSize == false )
			{
				done = true;
			}

		}
		return sdpStrRet;
	}

	function enhanceSDP(sdpStr, enhanceData)
	{
		var sdpLines = sdpStr.split(/\r\n/);
		var sdpSection = 'header';
		var hitMID = false;
		var sdpStrRet = '';

		//console.log("Original SDP: "+sdpStr);

		// Firefox provides a reasonable SDP, Chrome is just odd
		// so we have to doing a little mundging to make it all work
		if ( !sdpStr.includes("THIS_IS_SDPARTA") || videoChoice.includes("VP9") )
		{
			for(var sdpIndex in sdpLines)
			{
				var sdpLine = sdpLines[sdpIndex];

				if (sdpLine.length <= 0)
					continue;

				var doneCheck = checkLine(sdpLine);
				if ( !doneCheck )
					continue;

				sdpStrRet +=sdpLine;
				sdpStrRet += '\r\n';

			}
			if (_this.hasAudio) {
				sdpStrRet =  addAudio(sdpStrRet, deliverCheckLine(audioChoice,"audio"));
			}
			sdpStrRet =  addVideo(sdpStrRet, deliverCheckLine(videoChoice,"video"));
			sdpStr = sdpStrRet;
			sdpLines = sdpStr.split(/\r\n/);
			sdpStrRet = '';
		}

		for(var sdpIndex in sdpLines)
		{
			var sdpLine = sdpLines[sdpIndex];

			if (sdpLine.length <= 0)
				continue;

			if ( sdpLine.indexOf("m=audio") ==0 && audioIndex !=-1 )
			{
				audioMLines = sdpLine.split(" ");
				sdpStrRet+=audioMLines[0]+" "+audioMLines[1]+" "+audioMLines[2]+" "+audioIndex+"\r\n";
				continue;
			}

			if ( sdpLine.indexOf("m=video") == 0 && videoIndex !=-1 )
			{
				audioMLines = sdpLine.split(" ");
				sdpStrRet+=audioMLines[0]+" "+audioMLines[1]+" "+audioMLines[2]+" "+videoIndex+"\r\n";
				continue;
			}

			sdpStrRet += sdpLine;

			if (sdpLine.indexOf("m=audio") === 0)
			{
				sdpSection = 'audio';
				hitMID = false;
			}
			else if (sdpLine.indexOf("m=video") === 0)
			{
				sdpSection = 'video';
				hitMID = false;
			}
			else if (sdpLine.indexOf("a=rtpmap") == 0 )
			{
				sdpSection = 'bandwidth';
				hitMID = false;
			}

			if (sdpLine.indexOf("a=mid:") === 0 || sdpLine.indexOf("a=rtpmap") == 0 )
			{
				if (!hitMID)
				{
					if (_this.hasAudio && 'audio'.localeCompare(sdpSection) == 0)
					{
						
						if (enhanceData.audioBitrate !== undefined)
						{
							sdpStrRet += '\r\nb=CT:' + (enhanceData.audioBitrate);
							sdpStrRet += '\r\nb=AS:' + (enhanceData.audioBitrate);
						}
						hitMID = true;
						
					}
					else if ('video'.localeCompare(sdpSection) == 0)
					{
						if (enhanceData.videoBitrate !== undefined)
						{
							sdpStrRet += '\r\nb=CT:' + (enhanceData.videoBitrate);
							sdpStrRet += '\r\nb=AS:' + (enhanceData.videoBitrate);
							if ( enhanceData.videoFrameRate !== undefined )
								{
									sdpStrRet += '\r\na=framerate:'+enhanceData.videoFrameRate;
								}
						}
						hitMID = true;
					}
					else if ('bandwidth'.localeCompare(sdpSection) == 0 )
					{
						var rtpmapID;
						rtpmapID = getrtpMapID(sdpLine);
						if ( rtpmapID !== null  )
						{
							var match = rtpmapID[2].toLowerCase();
							if ( ('vp9'.localeCompare(match) == 0 ) ||  ('vp8'.localeCompare(match) == 0 ) || ('h264'.localeCompare(match) == 0 ) ||
								('red'.localeCompare(match) == 0 ) || ('ulpfec'.localeCompare(match) == 0 ) || ('rtx'.localeCompare(match) == 0 ) )
							{
								if (enhanceData.videoBitrate !== undefined)
									{
									sdpStrRet+='\r\na=fmtp:'+rtpmapID[1]+' x-google-min-bitrate='+(enhanceData.videoBitrate)+';x-google-max-bitrate='+(enhanceData.videoBitrate);
									}
							}

							if ( ('opus'.localeCompare(match) == 0 ) ||  ('isac'.localeCompare(match) == 0 ) || ('g722'.localeCompare(match) == 0 ) || ('pcmu'.localeCompare(match) == 0 ) ||
									('pcma'.localeCompare(match) == 0 ) || ('cn'.localeCompare(match) == 0 ))
							{
								if (enhanceData.audioBitrate !== undefined)
									{
									sdpStrRet+='\r\na=fmtp:'+rtpmapID[1]+' x-google-min-bitrate='+(enhanceData.audioBitrate)+';x-google-max-bitrate='+(enhanceData.audioBitrate);
									}
							}
						}
					}
				}
			}
			sdpStrRet += '\r\n';
		}
		console.log("Resuling SDP: "+sdpStrRet);
		return sdpStrRet;
	}

	function deliverCheckLine(profile,type)
	{
		var outputString = "";
		for(var line in SDPOutput)
		{
			var lineInUse = SDPOutput[line];
			outputString+=line;
			if ( lineInUse.includes(profile) )
			{
				if ( profile.includes("VP9") || profile.includes("VP8"))
				{
					var output = "";
					var outputs = lineInUse.split(/\r\n/);
					for(var position in outputs)
					{
						var transport = outputs[position];
						if (transport.indexOf("transport-cc") !== -1 || transport.indexOf("goog-remb") !== -1 || transport.indexOf("nack") !== -1)
						{
							continue;
						}
						output+=transport;
						output+="\r\n";
					}

					if (type.includes("audio") )
					{
						audioIndex = line;
					}

					if (type.includes("video") )
					{
						videoIndex = line;
					}

					return output;
				}
				if (type.includes("audio") )
				{
					audioIndex = line;
				}

				if (type.includes("video") )
				{
					videoIndex = line;
				}
				return lineInUse;
			}
		}
		return outputString;
	}

	function checkLine(line)
	{
		if ( line.startsWith("a=rtpmap") || line.startsWith("a=rtcp-fb") || line.startsWith("a=fmtp"))
		{
			var res = line.split(":");

			if ( res.length>1 )
			{
				var number = res[1].split(" ");
				if ( !isNaN(number[0]) )
				{
					if ( !number[1].startsWith("http") && !number[1].startsWith("ur") )
					{
						var currentString = SDPOutput[number[0]];
						if (!currentString)
						{
							currentString = "";
						}
						currentString+=line+"\r\n";
						SDPOutput[number[0]]=currentString;
						return false;
					}
				}
			}
		}

		return true;
	}

	function getrtpMapID(line)
	{
		var findid = new RegExp('a=rtpmap:(\\d+) (\\w+)/(\\d+)');
		var found = line.match(findid);
		return (found && found.length >= 3) ? found: null;
	}

	function errorHandler(error)
	{
        window.werror = error;
        console.log(JSON.stringify(error));
		console.log(error);
	}


}

function SmartUniStreamer(options) {

	var teacherRtc = null,
		presentationRtc = null,
		started = false;

	teacherRtc = new WebRTCStreamer({
		localVideoId: 'teacherVideo',
		applicationName: 'webrtc_origin',
		// applicationName: 'lowdemo3',
		streamName: options.teacher_stream_id,
		sourceChooserBtn: 'source_chooser_btn_teacher',
		stateChangeCallback: stateChangeListener
	});

	if (options.presentation_stream_id) {
		presentationRtc = new WebRTCStreamer({
			localVideoId: 'presentationVideo',
			applicationName: 'pres_origin',
			streamName: options.presentation_stream_id,
			presentation: true,
			sourceChooserBtn: 'source_chooser_btn_presentation'
		});
	}

	let _this = this;
	let startTime = null;

	var startButton = $('#' + options.startButton),
		streamInfoLine = $('#stream_info_line'),
		streamElapsedTime = $('#stream_elapsed_time');

	startButton.bind('click', startButtonClick);

	function stateChangeListener(newState) {
		console.log('Got new state: ' + newState);
		// Első indítás, menteni a stream-be és állapotot váltani
		if (!started && newState == 'connected') {
			axios.post('/lessons/'+lesson_id+'/stream/start', {})
				.then(function(resp) {
					started = true;
				});
		}
	}

	function startButtonClick() {

		let teacherStatus = teacherRtc.getStatus(),
			presentationStatus = presentationRtc ? presentationRtc.getStatus() : null;

		if (presentationRtc) {
			if (teacherStatus == 'ready' || presentationStatus == 'ready') {
				alert('Nincs forrás kiválasztva!');
			} else if (teacherStatus == 'binded' && presentationStatus == 'binded') {
				_this.start();
			} else if (teacherStatus == 'streaming' || presentationStatus == 'streaming') {
				_this.stop();
			}
		} else {
			if (teacherStatus == 'ready') {
				alert('Nincs forrás kiválasztva!');
			} else if (teacherStatus == 'binded') {
				_this.start();
			} else if (teacherStatus == 'streaming') {
				_this.stop();
			}
		}
		

	}

	var statusInterval = setInterval(updateStreamStatus, 500);

	this.start = function() {

		if (startTime == null) {
			startTime = new Date();
		}

		teacherRtc.startPublisher();
		(presentationRtc && presentationRtc.startPublisher());
	}

	this.stop = function() {
		teacherRtc.stopPublisher();
		(presentationRtc && presentationRtc.stopPublisher());

		axios.post('/lessons/'+lesson_id+'/stream/stop', {})
			.then(function(resp) {
			});
	}

	this.selectSource = function(stream, source, btn) {
		let rtc = stream == 1 ? teacherRtc : presentationRtc;
		
		rtc.bind(source == 2, btn);
	}

	function updateStreamStatus() {
		let teacherStatus = teacherRtc.getStatus(),
			presentationStatus = presentationRtc ? presentationRtc.getStatus() : null;

		if (presentationRtc && (teacherStatus == 'streaming' || presentationStatus == 'streaming') || teacherStatus == 'streaming') {

			startButton.find('i').removeClass('mdi-play-circle');
			startButton.find('i').addClass('mdi-pause');
			startButton.find('span').html('Stream leállítása');

			streamInfoLine.removeClass('hidden');

			let elapsedSec = Math.floor((new Date().getTime() - startTime.getTime()) / 1000);
			let diff = moment().diff(startTime);

			streamElapsedTime.html(moment(diff).format('mm:ss'));


		} else {
			startButton.find('i').removeClass('mdi-pause');
			startButton.find('i').addClass('mdi-play-circle');
			startButton.find('span').html('Stream indítása');
			streamInfoLine.addClass('hidden');
		}
	}
	

}



function initSmartUniStreamer(teacher_stream_id, presentation_stream_id) {
	
	smartUniStreamer = new SmartUniStreamer({
		teacher_stream_id,
		presentation_stream_id,
		controller: 'streamController',
		startButton: 'steam_start_btn',
		// stopButton: 'btn_stop'
	});

}