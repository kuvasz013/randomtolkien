function SmartEduLogger(level) {
    const levels = {
        DEBUG: 1,
        INFO: 2,
        ERROR: 3
    };

    const log_level = levels[level];

    this.debug = function(message, param) {
        log(levels.DEBUG, message, param);
    }

    this.info = function(message, param) {
        log(levels.INFO, message, param);
    }

    this.error = function(message, param) {
        log(levels.ERROR, message, param);
    }

    function log(level, message, param) {
        if (level >= log_level) {
            let text_level = level == levels.DEBUG ? 'DEBUG' : (level == levels.INFO ? 'INFO' : 'ERROR');
            if (typeof(param) != 'undefined') {
                console.log(text_level + ' - ' + message, param);
            } else {
                console.log(text_level + ' - ' + message);
            }
        }
        
    }
}
let seLogger = new SmartEduLogger('DEBUG');
