window.addEventListener('DOMContentLoaded', (event) => {

    let touConsent = document.getElementById("privacy-policy-consent");
    let privacyConsent = document.getElementById("tou-consent");
    let submit = document.getElementById("submit-registration");

    let touConsentChecked = false;
    let privacyConsentChecked = false;

    function updateSubmit() {
        submit.disabled = !touConsentChecked || !privacyConsentChecked;
    }

    if(typeof submit !== "undefined" && submit != null) {
        submit.disabled = true;
    
        touConsent.addEventListener('change', (event) => {
            if (touConsent.checked) {
                touConsentChecked = true;
            } else {
                touConsentChecked = false;
            }
            updateSubmit();
        });
    
        privacyConsent.addEventListener('change', (event) => {
            if (privacyConsent.checked) {
                privacyConsentChecked = true;
            } else {
                privacyConsentChecked = false;
            }
            updateSubmit();
        });
    }
    
});