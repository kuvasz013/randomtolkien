/*function addQuestion(btn, data) {
    let template = getTemplate("survey-question");
    $("#questions").append(template);

    if (data) {
        template.attr("data-id", data.id);
        template.find("input.question").val(data.question);
    }

    
    let mandatoryId = data ? 'ma' + data.id : getRandomId();
    template.find('.switch-mandatory').attr('id', mandatoryId).attr('name', mandatoryId);
    template.find('.label-mandatory').attr('for', mandatoryId);
    if (data) {
        template.find('.switch-mandatory').get(0).checked = data.mandatory;
    }
    

    let multipleId = data ? "mu" + data.id : getRandomId();
    template
        .find(".switch-multiple")
        .attr("id", multipleId)
        .attr("name", multipleId);
    template.find(".label-multiple").attr("for", multipleId);
    if (data) {
        template.find(".switch-multiple").attr("checked", data.multiple);
    }

    template.find(".switchery").each(function (idx, obj) {
        new Switchery($(this)[0], $(this).data());
    });

    if (data && data.answers) {
        let btn = template.find("a.add-answer-link").get(0);
        data.answers.forEach((answer) => {
            addAnswer(btn, answer);
        });
    }
}

function duplicateQuestion(btn) {
    let original = $(btn).closest(".survey-question");
    let question = original.clone();
    original.after(question);
    question.removeAttr("data-id");
}

function getRandomId() {
    return "i" + Math.round(Math.random() * 100000);
}

function removeQuestion(btn) {
    $(btn).closest(".card").remove();
}

function addAnswer(btn, data) {
    let body = $(btn).closest(".card-body");
    let template = getTemplate("survey-answer");

    if (data) {
        template.attr("data-id", data.id);
        template.find("input").val(data.answer);
        if (data.correct) {
            template.find(".correct-answer-toggle").addClass("checked");
        }
    }

    body.find(".answers").append(template);
}

function removeAnswer(btn) {
    $(btn).closest(".survey-answer").remove();
}

function toggleCorrect(btn) {
    let question = $(btn).closest(".survey-question");

    // Prevent multiple selection if switch is not checked
    if (
        !$(question).find(".switch-multiple").is(":checked") &&
        $(question).find(".correct-answer-toggle.checked").length !== 0 &&
        !$(btn).is(".checked")
    ) {
        return;
    }

    $(btn).toggleClass("checked");
}

function getTemplate(selector) {
    let template = $("#survey_templates")
        .find("." + selector)
        .clone();
    return template;
}

function createSurvey() {
    let survey = serializeSurvey();

    for (let i in survey.questions) {
        let question = survey.questions[i];
        if (question.answers.filter((a) => a.correct).length <= 0) {
            Notification.info("Adjon meg legalább egy helyes választ!");
            return;
        } else if (
            !question.multiple &&
            question.answers.filter((a) => a.correct).length > 1
        ) {
            Notification.info("Egyetlen helyes választ adjon meg!");
            return;
        }
    }

    axios
        .post("/surveys/create", {
            survey,
        })
        .then(function (resp) {
            if (resp.data.error) {
                Notification.info("Adjon meg legalább egy helyes választ!");
            } else if (resp) {
                location.href = "/surveys/" + resp.data.id + "/edit";
                console.log(resp.data);
            }
        })
        .catch(function (err) {
            console.error(err);
            Notification.error("Hiba a mentés közben!");
        });
}

function saveSurvey() {
    let survey = serializeSurvey();

    for (let i in survey.questions) {
        let question = survey.questions[i];
        if (question.answers.filter((a) => a.correct).length <= 0) {
            Notification.info("Adjon meg legalább egy helyes választ!");
            return;
        } else if (
            !question.multiple &&
            question.answers.filter((a) => a.correct).length > 1
        ) {
            Notification.info("Egyetlen helyes választ adjon meg!");
            return;
        }
    }

    axios
        .post("/surveys/" + survey.id + "/update", {
            survey,
        })
        .then(function (resp) {
            if (resp) {
                console.log(resp.data);
                Notification.success("A tesztet elmentettük!");
            }
        })
        .catch(function (err) {
            console.error(err);
            Notification.error("Hiba a mentés közben!");
        });
}

function serializeSurvey() {
    let survey = {
        id: $("#survey_id").val(),
        name: $("#name").val(),
        course_id: $("#course_id").val() || null,
        lesson_id: $("#lesson_id").val() || null,
        description: $("#description").val(),
        questions: [],
    };

    $("#questions .survey-question").each(function () {
        let q = $(this);
        let question = {
            id: q.attr("data-id") || null,
            question: q.find("input.question").val(),
            // mandatory: q.find('.switch-mandatory').is(':checked'),
            multiple: q.find(".switch-multiple").is(":checked"),
            answers: [],
        };

        q.find(".survey-answer").each(function () {
            let a = $(this);
            let answer = {
                id: a.attr("data-id") || null,
                answer: a.find("input.answer").val(),
                correct: a.find(".correct-answer-toggle").hasClass("checked"),
            };
            question.answers.push(answer);
        });

        survey.questions.push(question);
    });

    return survey;
}

function serializeSurveySubmit() {
    let submit = {
        lesson_id: lesson_id,
        survey_id: $("#survey").attr("data-id"),
        answers: [],
    };
    $(".question").each(function () {
        let question = $(this);
        let answer = {
            question_id: question.attr("data-id"),
            answers: [],
        };

        question.find("input:checked").each(function () {
            answer.answers.push($(this).attr("data-id"));
        });

        submit.answers.push(answer);
    });

    return submit;
}

function initQuestions() {
    questions.forEach((question) => {
        addQuestion(null, question);
    });
}

function finishSurvey() {
    let submit = serializeSurveySubmit();

    axios
        .post("/surveys/" + submit.survey_id + "/submit", {
            submit,
        })
        .then(function (resp) {
            if (resp) {
                console.log(resp.data);

                $(".questions").addClass("hidden");
                $("#submit_result h2").html(
                    Math.round(parseFloat(resp.data.result)) + "%"
                );
                $("#submit_result").removeClass("hidden");
                $(".submit-controls").addClass("hidden");
            }
        })
        .catch(function (err) {
            console.error(err);
        });
}

function deleteSurvey(survey_id) {
    axios
        .delete(`/surveys/${survey_id}`)

        .then((response) => {
            if (response.data.deleted === survey_id) {
                $(`#survey_${survey_id}`).remove();
                Notification.success("A tesztet töröltük!");
            }
        })

        .catch((error) => {
            console.log(error);
            Notification.error("Sikertelen törlés!");
        });
}

if (typeof questions != "undefined") {
    $(initQuestions);
}
*/