function addNote(noteContent) {
    let note = {
        note: $(noteContent).val().trim(),
        lesson_id: lesson_id
    };

    axios.post(`/notes`, note)

        .then(resp => {
            let note_element = createNoteFromData(resp.data);
            $('.note-list').prepend($(note_element));
            resizeTextArea($(note_element).find('.note-content')[0]);
            $(noteContent).val('');

            console.log(`Note created: ${resp.data.id}`);
        })

        .catch(err => {
            console.error(err);
            Notification.error('Sikertelen mentés!');
        });

    return false;
}


function updateNote(button) {

    let noteItem = $(button).closest('.note-item');

    $(noteItem).children('.note-content')
        .attr('readonly', 'readonly');

    $(noteItem)
        .removeClass('editable')
        .data('previous', '');

    let noteId = noteItem.attr('data-noteId');

    let note = {
        id: noteId,
        note: noteItem.children('.note-content').val().trim()
    };

    axios.put(`/notes/${noteId}`, note)

        .then(resp => {
            console.log(`Note updated: ${noteId}`);
            $(noteItem).data('previous', '');
        })

        .catch(err => {
            console.error(err);
            Notification.error('Sikertelen mentés!');
        });

    return false;
}

function editNote(button) {
    let noteItem = $(button).closest('.note-item');

    $(noteItem).children('.note-content')
        .removeAttr('readonly');
        
    $(noteItem)
        .addClass('editable')
        .data('previous', $(noteItem).children('.note-content').val().trim());

    return false;
}

function cancelNote(button) {
    let noteItem = $(button).closest('.note-item');

    $(noteItem).children('.note-content')
        .attr('readonly', 'readonly')
        .val($(noteItem).data('previous'));

    $(noteItem)
        .removeClass('editable')
        .data('previous', '');

    resizeTextArea($(noteItem).children('.note-content'));

    return false;
}

function deleteNote(button) {

    let noteId = $(button).closest('.note-item').attr('data-noteId');

    axios.delete(`/notes/${noteId}`)

        .then(resp => {
            $(button).closest('.note-item').remove();
            Notification.success('Jegyzet törölve!');
        })

        .catch(err => {
            console.error(err);
            Notification.error('Sikertelen törlés!');
        });

    return false;
}

function createNoteFromData(data) {
    let note = $('#note-template .note-item').first().clone();

    $(note).attr('data-noteId', data.id);
    $(note).find('.note-content').text(data.note || '');
    $(note).find('.note-date').text(moment(data.created).format('HH:mm:ss'));

    return note;
}

function resizeTextArea(textarea) {

    let scrollTop = $(document).scrollTop();

    textarea.style.height = '0px';
    textarea.style.height = (textarea.scrollHeight + 10) + 'px';
    
    $(document).scrollTop(scrollTop);

    return false;
}

function initNotes() {
    $('textarea.note-content').each(function () {

        // Resize textareas on page load
        resizeTextArea(this);

        // Save note with Ctrl+Enter
        $(this).on('keypress', e => {
            if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
                $(this).siblings('.note-actions').children('.note-save').click();
            }
        });

        // Resize textarea to fit content
        $(this).on('input', e => {
            resizeTextArea(this);
        });
    });
}

$(function () {
    if ($('#note-tab').length > 0) {
        $('#note-tab').on('shown.bs.tab', function (e) {
            initNotes();
        });
    }
});