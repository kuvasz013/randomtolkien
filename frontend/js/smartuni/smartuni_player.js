
let p = null,
    p1 = null,
    pr = null,
    pBtn = null,
    ptD = null,
    ptC = null;

let logSet = null;

let pipMode = 1,
    pipPosition = 1;

$(function() {
    p = $('#cf0');
    p1 = $('#cf1');
    pr = $('#player_progress div');
    pBtn = $('#player_play_btn');
    ptD = $('#player_time_duration');
    ptC = $('#player_time_current');

    $("#su_player").on('mouseenter', function() {
        $('#su_player_footer').fadeIn();
    });

    $("#su_player").on('mouseleave', function() {
        $('#su_player_footer').fadeOut();
    });

});

let loaded = 0;

if (document.getElementById('cf0')) {

    document.getElementById('cf0').addEventListener('loadedmetadata', function(a,b,c,d) {
        timeUpdated();
        $('#player_volume .progress-bar').css({width: '80%'});
        p.get(0).volume = 0.8;

        loaded += 1;
        updateLayout();

    });

    document.getElementById('cf1').addEventListener('loadedmetadata', function() {
        loaded += 1;
        updateLayout();
    });

    document.getElementById('cf0').addEventListener('timeupdate', timeUpdated);
}

function getVideoSize(video) {

    let vjs = video == 'teacher' ? $('#cf0 .vjs-tech').get(0) : $('#cf1 .vjs-tech').get(0);
    console.log(vjs);
    return {width: vjs.videoWidth, height: vjs.videoHeight};
    
    
}

function updateLayout() {

    if (loaded != 2) return;

    let teacherSize = getVideoSize('teacher'),
        presSize = getVideoSize('presentation'),
        teacherContainer = $('#cf0'),
        presContainer = $('#cf1');

    let playerContainer = $('#su_player');

    if (playerContainer.width() > 1000) {
        layoutParallel({ teacherSize, presSize, teacherContainer, presContainer, playerContainer });
        $('#pip_container').hide();
    } else {
        layoutPIP({ teacherSize, presSize, teacherContainer, presContainer, playerContainer });
        $('#pip_container').show();
    }

}

function layoutPIP(params) {

    let pipMode = 2; // 1 teacher, 2 presentation in large

    let v1 = {
        size: pipMode == 1 ? params.teacherSize : params.presSize,
        container: pipMode == 1 ? params.teacherContainer : params.presContainer
    };

    let v2 = {
        size: pipMode == 2 ? params.teacherSize : params.presSize,
        container: pipMode == 2 ? params.teacherContainer : params.presContainer
    };

    let mainHeight = 400;

    params.playerContainer.height(mainHeight + 'px');

    let ratio1 = v1.size.height / v1.size.width,
        ratio2 = v2.size.height / v2.size.width;

    let v1Width = Math.round(mainHeight / ratio1),
        v1Left = Math.round((params.playerContainer.width() - v1Width) / 2);

    v1.container.width(v1Width + 'px');
    v1.container.height(mainHeight + 'px');
    v1.container.css({left: v1Left, top: 0});

    let v2Height = mainHeight * 0.4,
        v2Width = v2Height / ratio2;

    let v2Left = v1Left + v1Width - v2Width,
        v2Top = pipPosition == 1 ? 0 : (mainHeight - v2Height);

    v2.container.width(v2Width + 'px');
    v2.container.height(v2Height + 'px');
    v2.container.css({left: v2Left, top: v2Top});

    v1.container.css('zIndex', '100');
    v2.container.css('zIndex', '110');

}

function layoutParallel(params) {

    // t 320 x 240  0.75
    // p 416 x 240  0.57

    // a 736 x 240  0.326

    // $('#su_player').height(mainHeight + 'px');

    let tRatio = params.teacherSize.height / params.teacherSize.width,
        pRatio = params.presSize.height / params.presSize.width;

    let refHeight = 400;
    let fullRatio = refHeight / (refHeight / tRatio + refHeight / pRatio);
    let mainHeight = params.playerContainer.width() * fullRatio;

    $('#su_player').height(mainHeight + 'px');

    // console.log(params);

    let tHeight = mainHeight,
        tWidth = mainHeight / tRatio;

    let pHeight = mainHeight,
        pWidth = mainHeight / pRatio;

    // console.log({tWidth, pWidth, pRatio, tRatio});


    let tLeft = Math.round((params.playerContainer.width() - (tWidth + pWidth)) / 2),
        pLeft = tLeft + tWidth;

    let tTop = 0,
        pTop = 0;

    // console.log({ tLeft, tTop, pLeft, pTop });

    params.teacherContainer.height(tHeight + 'px');
    params.teacherContainer.width(tWidth + 'px');
    params.teacherContainer.css({top: tTop, left: tLeft});

    params.presContainer.height(pHeight + 'px');
    params.presContainer.width(pWidth + 'px');
    params.presContainer.css({top: pTop, left: pLeft});

}

/*
document.getElementById('cf0').addEventListener('loadeddata', function(a,b,c,d) {
    console.log('onloaded data'); 
    console.log(a); 
    console.log(b); 
    console.log(c); 
    console.log(d); 
});
*/




function timeUpdated() {
    if (p != null) {
        let current = p.get(0).currentTime;
        let duration = p.get(0).duration;
        let percent = 0;
        if (isNaN(duration)) {
            duration = 0;
        } else {
            percent = current / (duration/100.0);
        }
        
        pr.css({
            width: percent+'%'
        });

        if (logSet !== null) {
            logSet.add(Math.round(current));
        }

        ptD.html(moment.unix(duration).format('mm:ss'));
        ptC.html(moment.unix(current).format('mm:ss'));

    }
}


function playToggle(e) {

    if (logSet === null) {
        logSet = new Set();
        setInterval(doLog, 10000);
    }

    if (p.get(0).paused) {
        document.getElementById('cf0').play();
        document.getElementById('cf1').play();
    } else {
        document.getElementById('cf0').pause();
        document.getElementById('cf1').pause();
    }

    updateControls();
}


function cfPlay() {
    document.getElementById('cf0').play();
    document.getElementById('cf1').play();

    updateControls();
}

function cfPause() {
    document.getElementById('cf0').pause();
    document.getElementById('cf1').pause();

    updateControls();
}

function updateControls() {

    if (p.get(0).paused) {
        pBtn.find('i').addClass('fa-play');
        pBtn.find('i').removeClass('fa-pause');
    } else {
        pBtn.find('i').removeClass('fa-play');
        pBtn.find('i').addClass('fa-pause');
    }

}

function setVolume(e) {
    let boxLeft = $(e.target).offset().left;
    let boxWidth = 50;
    let percent = Math.round((e.pageX - boxLeft) / 0.5);

    $('#player_volume .progress-bar').css({width: percent+'%'});
    p.get(0).volume = percent / 100;
    
}

function seekPlayer(e) {
    let d = $("#player_progress");
    let boxLeft = d.offset().left;
    let boxWidth = d.width();
    let duration = p.get(0).duration;
    let percent = Math.round((e.pageX - boxLeft) / (boxWidth/100));
    let seek_pos = duration * (percent/100);

    p.get(0).currentTime = seek_pos;
    p1.get(0).currentTime = seek_pos;
    
}


function doLog() {

    /*
    if (logSet.size > 0) {

        axios.post('/stream/log', {
            stream_id,
            times: [...logSet].join(',')
        }).then(function(resp) {
            logSet.clear();
        }).catch(function(err) {
            console.error(err);
        });

    }
    */
}

function changePipPosition(link) {

    pipPosition = $(link).find('.mdi').hasClass('mdi-picture-in-picture-top-right') ? 1 : 2;

    $('#controls_right_side .pip-pos-link i').removeClass('text-primary');
    $(link).find('i').addClass('text-primary');

    updateLayout();

}

$(window).resize(function() {
    updateLayout();
});