function submitQuestion() {
    let input = $('.question-form input');

    if ($(input).val() === '') return;

    let question = { question: $(input).val().trim() };

    axios.post(`/lessons/${lesson_id}/questions`, question)
    
        .then(resp => {

            
            /*
            $('.question-container')
                .append($(createQuestionFromData(resp.data)))
                .animate({ scrollTop: $('.question-container')
                .prop("scrollHeight")}, 1000);
                */
                
            $(input).val('');
        })
        
        .catch(function (err) {
            console.error(err);
            Notification.error('Sikertelen beküldés!');
        });
}

function createQuestionFromData(data) {
    let question = $('#question-template .question').first().clone();

    question.attr('id', 'q_'+data.id);
    $(question).children('.author').text(`${data.lastname} ${data.firstname}`);
    $(question).children('p').text(data.question);
    $(question).find('.time')
        .attr('title', moment(data.created).format('HH:mm:ss'))
        .text(moment(data.created).format('HH:mm'));

    return question;
}

function questionReceived(data) {
    console.log('Questions received: ', data);
    console.log('Self user: ', user_id);
    if (data.user_id != user_id && user_role == 'student') return;
    if ($('#q_'+data.id).length > 0) return;

    $('.question-container')
        .append($(createQuestionFromData(data)))
        .animate({ scrollTop: $('.question-container').prop("scrollHeight")}, 1000);

}

$(function() {
    if ($('.question-container').length > 0) {

        // Send form if Enter is pressed - prevent behaviour if Shift is also pressed
        $('.question-form').on("keypress", e => {
            if ((e.keyCode == 13 || e.keyCode == 10) && !e.shiftKey) {
                submitQuestion();
            }
        });

        // Set scroll to the bottom of the question container when tab is shown
        $('#question-tab').on('shown.bs.tab', function(e) {
            $('.question-container').scrollTop($('.question-container')[0].scrollHeight);
        });

    }
});
