

function Notification() {

    let position = 'bottom-right',
        stack = 1,
        hideAfter = 3000;

    this.success = function(title, text) {
        let options = {
            heading: title,
            text: text,
            position: position,
            loaderBg: '#5ba035',
            icon: 'success',
            hideAfter: hideAfter,
            stack: stack
        };
        showToast(options);
    }

    this.info = function(title, text) {
        let options = {
            heading: title,
            text: text,
            position: position,
            loaderBg: '#3b98b5',
            icon: 'info',
            hideAfter: hideAfter,
            stack: stack
        };
        showToast(options);
    }

    this.warning = function(title, text) {
        let options = {
            heading: title,
            text: text,
            position: position,
            loaderBg: '#da8609',
            icon: 'warning',
            hideAfter: hideAfter,
            stack: stack
        };
        showToast(options);
    }

    this.error = function(title, text) {
        let options = {
            heading: title,
            text: text,
            position: position,
            loaderBg: '#bf441d',
            icon: 'error',
            hideAfter: hideAfter,
            stack: stack
        };
        showToast(options);
    }

    this.unknownError = (err) => {
        console.error(err);
        this.error('Hiba', 'Ismeretlen hiba történt.');
    }

    function showToast(options) {
        $.toast().reset('all');
        $.toast(options);
    }

}

var Notification = new Notification();