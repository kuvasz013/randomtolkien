/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-console */

function showStartLesson() {
    $('#start_lesson_modal').modal('show');
}

function startLesson(type, stream_count) {

    // let record = $('#lesson_record').is(':checked');

    let stream = {
        stream_type: stream_count == 1 ? 'simple' : 'dual',
        // player_type: type == 1 ? 'webrtc' : 'hls',
        player_type: 'webrtc',
        record: false
    };

    axios.post('/lessons/'+lesson_id+'/start', stream)
        .then(function() {
            location.reload();
        });

}

function startLessonCustom(stream_type, player_type, record) {

    // let record = $('#lesson_record').is(':checked');

    let stream = {
        stream_type: stream_type,
        player_type: player_type,
        record: record
    };

    axios.post('/lessons/'+lesson_id+'/start', stream)
        .then(function() {
            location.reload();
        });

}

$(function () {

    if (typeof io === 'undefined') {
        console.log("io is not present, can't create websocket connection.")
        return;
    }

    if (typeof(lesson_id) == 'undefined') {
        return;
    }
    
    console.log('Connect to ' + `/?lesson_id=${lesson_id}`);
    lessonSocket = io(`/?lesson_id=${lesson_id}`, {
        path: '/lesson_socket'
    });

    lessonSocket.on('connect', () => {
        console.log(`Lesson socket connection established: ${lessonSocket.id}`);
    });

    lessonSocket.on('link_added_to_lesson', function (data) {
        $("#content_list_body").append(data);
    });

    lessonSocket.on('content_added_link', function (data) {
        /*let row = $('#link_template').clone();
        row.removeClass('hidden');
        row.find('.link-title').attr('href', data.url).attr('title', data.url).html(data.title);
        row.find('.subtitle').html(moment(data.created).format('L'));
        row.find('.link-edit').data('href', '/links/'+data.id+'/edit');
        row.find('.link-delete').data('href', '/links/'+data.id+'/delete');
        if (user_role == 'student') {
            row.find('.content-actions a').remove();
        }
        $('.content-list').prepend(row);
        */
       refreshContentList();
    });

    lessonSocket.on('contents_changed', function (data) {
       refreshContentList();
       notifyTabActivity('contents');
    });

    lessonSocket.on('link_removed_from_lesson', function (data) {
        $(`#${data}`).remove();
    });

    lessonSocket.on('poll_added_to_lesson', function (data) {
        $("#content_list_body").append(data);
    });

    lessonSocket.on('poll_removed_from_lesson', function (data) {
        $(`#${data}`).remove();
    });

    lessonSocket.on('attachment_added_to_lesson', function (data) {
        $("#content_list_body").append(data);
    });

    lessonSocket.on('attachment_removed_from_lesson', function (data) {
        $(`#${data}`).remove();
    });

    lessonSocket.on('survey_added_to_lesson', function (data) {
        $("#content_list_body").append(data);
    });

    lessonSocket.on('survey_removed_from_lesson', function (data) {
        $(`#${data}`).remove();
    });

    lessonSocket.on('message_received', function (data) {
        messageReceived(data);
        notifyTabActivity('messages');
    });

    lessonSocket.on('question_received', function (data) {
        questionReceived(data);
        notifyTabActivity('questions');
    });

});



function switchStream(type) {
    if (teacherStreamer && teacherStreamer.isPublishing()) {
        if (confirm('Módváltáshoz le kell állítanunk a mostani adást. Biztosan megteszi?')) {
            teacherStreamer.stop();
            setTimeout(function() {
                location.href='/lessons/'+lesson_id+'/switch?type='+type
            }, 1000);
            
        }
    } else {
        location.href='/lessons/'+lesson_id+'/switch?type='+type
    }
}

function refreshContentList() {
    axios.get('/lessons/'+lesson_id+'/room/contents')
        .then(function(resp) {
            Notification.success('Frissült az órai tartalom!');
            $('#lesson_tab_contents').html(resp.data);
        });
}