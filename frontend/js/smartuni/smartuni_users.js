function getUsersNotInCourse(course_id) {

    $('#modal-spinner').removeClass('fade');
    $('#modal-user-selector-table').addClass('fade');
    $('#modal-error').addClass('fade');

    axios.get(`/courses/${course_id}/users?invert=true`)

    .then(resp => {
        if (resp.data) {
            $('#modal-spinner').addClass('fade');
            $('#modal-user-selector-table').removeClass('fade').html(resp.data);
            $('#modal-error').addClass('fade');
        }
    })

    .catch(err => {
        console.error(err.response);
        $('#modal-spinner').addClass('fade');
        $('#modal-user-selector-tabel').addClass('fade');
        $('#modal-error').removeClass('fade');
    });
}

function addUsersToCourse(course_id) {
    let user_ids = $('#add-user-modal-save').data('userIdList');

    if (!user_ids || user_ids.length <= 0) {
        Notification.info('Nincs kiválasztott felhasználó');
        return;
    }

    axios.post(`/courses/${course_id}/users`, { user_ids })

    .then(resp => {
        let user_count = $('#user-list-table > tbody tr').length;
        $('#user-count').text(user_count);
        location.href = location.href.split('?')[0] + '?tab=participants';
    })

    .catch(err => {
        console.error(err.response);
        Notification.error('Sikertelen hozzáadás!');
    });
}

function selectUser(user_id) {
    let element = $('#select-user-' + user_id);
    let userIdList = $('#add-user-modal-save').data('userIdList') || [];

    if (element.hasClass('selected')) {
        userIdList.splice(userIdList.indexOf(user_id), 1);
    } else {
        userIdList.push(user_id);
    }

    $('#add-user-modal-save').data('userIdList', userIdList);
    element.toggleClass('selected');
}

function deleteUserFromCourse(course_id, user_id) {
    let element = $('#list-user-' + user_id);

    axios.delete(`/courses/${course_id}/users/${user_id}`)

    .then(resp => {
        element.remove();
        let user_count = $('#user-list-table > tbody tr').length;
        $('#user-count').text(user_count);
        Notification.success('Felhasználó sikeresen törölve!');
    })

    .catch(err => {
        console.error(err.response);
        if (err.response.status === 403) {
            Notification.error('Nem törölheti magát a kurzusról!');
        } else {
            Notification.error('Sikertelen törlés!');
        }
    });
}
