function submitMessage() {
    let input = $('.message-form textarea');

    if ($(input).val().trim() === '') {
        $(input).val('');
        return;
    }

    let message = { message: $(input).val().trim() };

    axios.post(`/lessons/${lesson_id}/messages`, message)
    
        .then(resp => {

            /*
            $('.message-container')
                .append($(createMessageFromData(resp.data)))
                .animate({ scrollTop: $('.message-container')
                .prop("scrollHeight")}, 1000);
                */

            $(input).val('');
            adjustHeight(input.get(0), 38);
        })
        
        .catch(function (err) {
            console.error(err);
            Notification.error('Sikertelen beküldés!');
        });
}

function createMessageFromData(data) {

    let message = $('#message_template .message').first().clone();

    message.attr('id', 'm_'+data.id);
    $(message).children('.author').text(`${data.lastname} ${data.firstname}`);
    $(message).children('p').html(data.message.replace(/\n/g, "<br>"));
    $(message).find('.time')
        .attr('title', moment(data.created).format('H:mm:ss'))
        .text(moment(data.created).format('H:mm'));

    if (['admin', 'teacher', 'agent'].includes(data.role)) {
        $(message).addClass('teacher');
    }

    return message;
}

function messageReceived(data) {

    if ($('#m_'+data.id).length > 0) return;

    $('.message-container')
        .append($(createMessageFromData(data)))
        .animate({ scrollTop: $('.message-container').prop("scrollHeight")}, 1000);

}

let chat_input = null;

$(function() {
    if ($('.message-container').length > 0) {

        chat_input = $('.message-form textarea');

        // Send form if Enter is pressed - prevent behaviour if Shift is also pressed
        $('.message-form textarea').on("keydown", e => {
            
            if ((e.keyCode == 13 || e.keyCode == 10) && !e.shiftKey) {
                if (chat_input.val().trim() == '') {
                    e.preventDefault();
                } else {
                    submitMessage();
                }
            } else {
                setTimeout(function() {
                    adjustHeight(chat_input.get(0), 38);
                }, 100);
                
            }
            
            
        });


        // Set scroll to the bottom of the message container when tab is shown
        $('#message-tab').on('shown.bs.tab', function(e) {
            $('.message-container').scrollTop($('.message-container')[0].scrollHeight);
        });

    }
});


function adjustHeight(el, minHeight) {
    
    // compute the height difference which is caused by border and outline
    var outerHeight = parseInt(window.getComputedStyle(el).height, 10);
    var diff = outerHeight - el.clientHeight;

    // set the height to 0 in case of it has to be shrinked
    el.style.height = 0;

    // set the correct height
    // el.scrollHeight is the full height of the content, not just the visible part
    let a = Math.max(minHeight, el.scrollHeight + diff);
    el.style.height = Math.min(a, 100) + 'px';
}