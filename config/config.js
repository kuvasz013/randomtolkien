const path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

const config = {
    local: {
        env: 'local',
        root: rootPath,
        proto: 'http',
        baseHost: 'local.smartuni.hu',
        landingHost: 'local.smartuni.hu',
        panelHost: 'localhost',
        port: 6500,
        logDir: path.join(rootPath, 'log'),
        accessLogFormat: 'tiny',
        dataDir: path.join(rootPath, 'data'),
        tmpDir: path.join(rootPath, 'tmp'),
        partialsDir: path.join(rootPath, 'app', 'views', 'partials'),
        database: {
            host: '127.0.0.1',
            database: 'smartuni',
            username: 'smartuni',
            password: 'smartuni',
            schema: 'public',
        },
        objectstore: {
            basePath: './data',
            bucket: 'smartedu-local',
            partSize: '10485760',
            queueSize: '2',
        },
    },
    deploy: {
        env: 'deploy',
        root: rootPath,
        proto: 'http',
        baseHost: 'local.smartuni.hu',
        landingHost: 'local.smartuni.hu',
        panelHost: 'kuvasz.party',
        port: 6500,
        logDir: path.join(rootPath, 'log'),
        accessLogFormat: 'tiny',
        dataDir: path.join(rootPath, 'data'),
        tmpDir: path.join(rootPath, 'tmp'),
        partialsDir: path.join(rootPath, 'app', 'views', 'partials'),
        database: {
            host: '127.0.0.1',
            database: 'smartuni',
            username: 'smartuni',
            password: 'smartuni',
            schema: 'public',
        },
        objectstore: {
            basePath: './data',
            bucket: 'smartedu-local',
            partSize: '10485760',
            queueSize: '2',
        },
    }
};

module.exports = config[env];
