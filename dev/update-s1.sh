#!/bin/sh

whoami

cd $1

git pull

npm install

npm run db:migrate:s1

npm run frontend:build

pm2 restart ecosystem.config.js --only smartuni-s1