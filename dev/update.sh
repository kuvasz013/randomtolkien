#!/bin/sh

whoami

cd $1

git pull origin master

npm install

npm run db:migrate:dev

npm run frontend:build

pm2 restart smartuni