const db = require('../../lib/db'),
    logger = require('../../lib/logger'),
    ep = require('./entityRepository');

const SUBMITS_TABLE = 'submits';
const LINKS_TABLE = 'links';

async function create(submit, trx) {
    let result = await ep.query(SUBMITS_TABLE, trx).returning('*').insert({
        name: submit.name,
    });

    return Promise.resolve(result[0]);
}

async function saveLinks(submit, submitId, trx) {
    try {
        for (let link of submit.links) {
            await ep.query(LINKS_TABLE, trx).returning('*').insert({
                submit_id: submitId,
                url: link,
            });
        }
        return Promise.resolve(true);
    } catch (err) {
        return Promise.reject(err);
    }
}

async function getLinks(submitId, trx) {
    return await ep.query(LINKS_TABLE).returning('*').where({submit_id: submitId});
}

async function getAllSubmits() {
    return await ep.query(SUBMITS_TABLE).returning('*');
}

async function getAllLinks() {
    return await ep.query(LINKS_TABLE).returning('*');
}

async function deleteAll(params, trx) {
    await ep.query(LINKS_TABLE, trx).where({}).del();
    logger.info('Links deleted!');

    await ep.query(SUBMITS_TABLE, trx).where({}).del();
    logger.info('Submits deleted!');

    return Promise.resolve(true);
}

module.exports = {
    create,
    saveLinks,
    deleteAll,
    getAllSubmits,
    getAllLinks,
    getLinks
};
