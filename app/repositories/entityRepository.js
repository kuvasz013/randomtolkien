const db = require('../../lib/db'),
    logger = require('../../lib/logger');


class DBTransaction {
    constructor(db) {
        this._db = db;
        this._trx = null;
    }

    start() {
        let _this = this;
        logger.debug("Start transaction");
        return new Promise((resolve, reject) => {
            _this._db.transaction(function(trx) {
                _this._trx = trx;
                resolve(true);
            });
        });
        
    }

    commit() {
        this._trx.commit();
    }

    rollback() {
        this._trx.rollback();
    }

    get trx() {
        return this._trx;
    }

    set trx(trx) {
        this._trx = trx;
    }
}

async function startTransaction() {
    let transaction = new DBTransaction(db);
    let started = await transaction.start();

    return transaction;
}

function query(table, trx) {
    let query = db(table);
    if (trx) {
        query.transacting(trx.trx);
    }
    return query;
}

module.exports = {
    startTransaction,
    query
};