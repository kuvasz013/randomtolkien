const express = require('express'),
    engine = require('ejs-blocks'),
    morgan = require('morgan'),
    moment = require('moment'),
    path = require('path'),
    useragent = require('express-useragent'),
    bodyParser = require('body-parser');

const publicCtrl = require('../controllers/publicCtrl'),
    adminCtrl = require('../controllers/adminCtrl');

const logger = require('../../lib/logger');

module.exports = (app, config) => {
    // EXPRESS
    app.use(useragent.express());
    app.set('views', path.join(__dirname, '../views'));
    app.engine('ejs', engine);
    app.set('view engine', 'ejs');
    app.use(express.static(path.join(__dirname, '../../public')));

    // BODYPARSER
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    // ACCESS LOG
    app.use(morgan(config.accessLogFormat));

    moment.locale('hu');

    
    app.use(async (req, res, next) => {
        res.locals.moment = moment;
        next();
    });

    // MAIN ROUTING
    app.use('/public', publicCtrl);
    app.use('/admin', adminCtrl);
};
