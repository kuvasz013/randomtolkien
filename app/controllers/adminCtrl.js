'use strict';

const express = require('express'),
    router = express.Router(),
    { StatusCodes } = require('http-status-codes'),
    moment = require('moment');

const logger = require('../../lib/logger'),
    submitRepository = require('../repositories/submitRepository');

router.get('/list', async (req, res) => {
    try {
        let submits = await submitRepository.getAllSubmits();
        let links = await submitRepository.getAllLinks();

        submits.map((s) => (s.links = []));

        links.forEach((link) => {
            submits.find((s) => s.id == link.submit_id).links.push(link);
        });

        submits.sort((a, b) => {
            if (a.links.length == b.links.length) {
                return moment(a.created).isBefore(moment(b.created)) ? -1 : 1;
            } else {
                return a.links.length - b.links.length
            }
        });

        //logger.info(JSON.stringify(submits, null, 4));

        res.render('tolkien/list', { submits });
    } catch (err) {
        logger.error(err.stack || err);
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
});

router.get('/links', async (req, res) => {
    try {
        let links = await submitRepository.getLinks(req.query.submit_id);
        return res.render('tolkien/links_modal', { links });
    } catch (err) {
        logger.error(err.stack || err);
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
});

router.delete('/reset', async (req, res) => {
    try {
        await submitRepository.deleteAll();
        return res.sendStatus(StatusCodes.OK);
    } catch (err) {
        logger.error(err.stack || err);
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
});

module.exports = router;
