'use strict';

const express = require('express'),
    router = express.Router(),
    { StatusCodes } = require('http-status-codes');

const logger = require('../../lib/logger'),
    submitRepository = require('../repositories/submitRepository'),
    entityRepository = require('../repositories/entityRepository');

router.get('/user', async (req, res) => {
    try {
        res.render('tolkien/user')
    } catch (err) {
        logger.error(err.stack || err);
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
});

router.get('/success', async (req, res) => {
    res.render('tolkien/success')
});

router.post('/submit', async (req, res) => {
    try {
        //logger.info(JSON.stringify(req.body, null, 4));

        let submit = await submitRepository.create(req.body);
        logger.info(JSON.stringify(submit, null, 4));
        await submitRepository.saveLinks(req.body, submit.id);

        return res.sendStatus(StatusCodes.CREATED);
    } catch (err) {
        logger.error(err.stack || err);
        res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
});

module.exports = router;
