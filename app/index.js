const createNamespace = require('continuation-local-storage').createNamespace,
    vrNamespace = createNamespace('hu.smartuni');

const express = require('express'),
    config = require('../config/config'),
    setup = require('./server/app'),
    logger = require('../lib/logger'),
    vhost = require('vhost');

// Setup applications
const app = express();
app.enable('trust proxy');
setup(app, config);

const expressApp = express(),
    http = require('http').createServer(expressApp);

expressApp.use(vhost(config.panelHost, app));

// Start main app
http.listen(config.port, '0.0.0.0', () => {
    logger.info(`Listening on port: ${config.port}`);
    logger.info(`Running in environment: ${config.env}`);
});
