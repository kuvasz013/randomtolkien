"use strict";

const gulp = require("gulp"),
    sass = require("gulp-sass"),
    sourcemaps = require("gulp-sourcemaps"),
    autoprefixer = require("gulp-autoprefixer"),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify-es").default,
    clean = require("gulp-rimraf"),
    lodash = require("lodash");

const folder = {
    src: "frontend/", // source files
    dist: "public/", // build files
    dist_assets: "public/" //build assets files
};

const folder_redesign = {
    src: "frontend_redesign/",
    dist: "public/",
    dist_assets: "public/"
}

// copy fonts from src folder to dist folder
function fonts() {
    var out = folder.dist_assets + "fonts/";

    return gulp.src([folder.src + "fonts/**/*"]).pipe(gulp.dest(out));
}

function css_redesign() {
    return gulp
        .src([folder_redesign.src + "scss/style.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(cleanCSS())
        .pipe(rename( { suffix: ".min" } ))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(folder.dist + "css/"))       
}

function img() {
    return gulp
        .src([folder_redesign.src + "img/**/*.png", folder_redesign.src + "img/**/*.svg"])
        .pipe(gulp.dest(folder_redesign.dist + 'img/'))
}

function js_redesign() {
    // gulp.src([folder_redesign.dist + 'js/*']).pipe(clean());

    return gulp
        .src([folder_redesign.src + 'js/**/*'])
        .pipe(sourcemaps.init())
        .pipe(concat('smartuni_redesign.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(folder_redesign.dist + 'js/'));
}

function fonts_redesign() {
    return gulp.src([folder_redesign.src + 'fonts/**/*']).pipe(gulp.dest(folder_redesign.dist_assets + '/fonts' ))
}

// compile & minify sass
function css() {
    gulp
        .src([folder.src + "/scss/bootstrap.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass()) // scss to css
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(gulp.dest(folder.dist_assets + "css/"))
        .pipe(cleanCSS())
        .pipe(
            rename({
                // rename app.css to icons.min.css
                suffix: ".min"
            })
        )
        .pipe(sourcemaps.write("./")) // source maps for icons.min.css
        .pipe(gulp.dest(folder.dist_assets + "css/"));
    gulp
        .src([folder.src + "/scss/icons.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass()) // scss to css
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(gulp.dest(folder.dist_assets + "css/"))
        .pipe(cleanCSS())
        .pipe(
            rename({
                // rename app.css to icons.min.css
                suffix: ".min"
            })
        )
        .pipe(sourcemaps.write("./")) // source maps for icons.min.css
        .pipe(gulp.dest(folder.dist_assets + "css/"));
    gulp
        .src([folder.src + "/scss/app-rtl.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass()) // scss to css
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(gulp.dest(folder.dist_assets + "css/"))
        .pipe(cleanCSS())
        .pipe(
            rename({
                // rename app.css to app.min.css
                suffix: ".min"
            })
        )
        .pipe(sourcemaps.write("./")) // source maps for app.min.css
        .pipe(gulp.dest(folder.dist_assets + "css/"));


    // libs
    gulp
        .src([folder.src + "/libs/**/*.css"])
        .pipe(sourcemaps.init())
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(cleanCSS())
        .pipe(concat("libs.min.css"))
        .pipe(gulp.dest(folder.dist_assets + "css/"));

    return gulp
        .src([folder.src + "/scss/app.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass()) // scss to css
        .pipe(
            autoprefixer({
                "overrideBrowserslist": [
                    "> 1%",
                    "ie >= 8",
                    "edge >= 15",
                    "ie_mob >= 10",
                    "ff >= 45",
                    "chrome >= 45",
                    "safari >= 7",
                    "opera >= 23",
                    "ios >= 7",
                    "android >= 4",
                    "bb >= 10"
                ]
            })
        )
        .pipe(gulp.dest(folder.dist_assets + "css/"))
        .pipe(cleanCSS())
        .pipe(
            rename({
                // rename app.css to app.min.css
                suffix: ".min"
            })
        )
        .pipe(sourcemaps.write("./")) // source maps for app.min.css
        .pipe(gulp.dest(folder.dist_assets + "css/"));

}

// js
function javascript() {
    var out = folder.dist_assets + "js/";

    // It's important to keep files at this order
    // so that `app.min.js` can be executed properly
    gulp
        .src([
            folder.src + "js/vendor/jquery.js",
            folder.src + "js/vendor/bootstrap.bundle.js",
            folder.src + "js/vendor/jquery.slimscroll.js",
            folder.src + "js/vendor/waves.js",
            folder.src + "js/vendor/metisMenu.js",
            folder.src + "js/vendor/jquery.waypoints.min.js",
            folder.src + "js/vendor/jquery.counterup.min.js"
        ])
        .pipe(sourcemaps.init())
        .pipe(concat("vendor.js"))
        .pipe(gulp.dest(out))
        .pipe(
            rename({
                // rename app.js to app.min.js
                suffix: ".min"
            })
        )
        .pipe(uglify())
        .on("error", function (err) {
            console.log(err.toString());
        })
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(out));


    // azért van itt is felsorolva kézzel, mert számít a sorrend bizonyos esetben és nem hagyatkozhatunk a sima abc sorrendre
    gulp
        .src([
            folder.src + "libs/axios.min.js",
            folder.src + "libs/jquery.mask.min.js",
            folder.src + "libs/select2/select2.min.js",
            folder.src + "libs/bootstrap-datepicker/bootstrap-datepicker.min.js",
            folder.src + "libs/bootstrap-datepicker/bootstrap-datepicker.hu.min.js",
            folder.src + "libs/switchery/switchery.min.js",
            folder.src + "libs/moment-with-locales.min.js",
            folder.src + "libs/jquery-toast/jquery.toast.min.js"
        ])
        .pipe(sourcemaps.init())
        .pipe(concat("libs.min.js"))
        .pipe(gulp.dest(out));

    gulp
        .src([
            folder.src + "js/smartuni/**/*"
        ])
        .pipe(sourcemaps.init())
        .pipe(concat("smartuni.js"))
        .pipe(gulp.dest(out))
        .pipe(rename({suffix: ".min"}))
        .pipe(uglify())
        .on("error", function (err) {
            console.log(err.toString());
        })
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(out));


    return gulp
        .src([
            folder.src + "js/app.js"
        ])
        .pipe(sourcemaps.init())
        .pipe(concat("app.js"))
        .pipe(gulp.dest(out))
        .pipe(
            rename({
                // rename app.js to app.min.js
                suffix: ".min"
            })
        )
        .pipe(uglify())
        .on("error", function (err) {
            console.log(err.toString());
        })
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(out));
}

// live browser loading
// function browserSync(done) {
//     browsersync.init({
//         server: {
//             baseDir: folder.dist
//         }
//     });
//     done();
// }

// function reloadBrowserSync(done) {
//     browsersync.reload();
//     done();
// }

// watch all changes
// function watchFiles() {
//     gulp.watch(folder.src + "html/**", gulp.series(html, reloadBrowserSync));
//     gulp.watch(folder.src + "assets/images/**/*", gulp.series(imageMin, reloadBrowserSync));
//     gulp.watch(folder.src + "assets/fonts/**/*", gulp.series(fonts, reloadBrowserSync));
//     gulp.watch(folder.src + "scss/**/*", gulp.series(css, reloadBrowserSync));
//     gulp.watch(folder.src + "js/**/*", gulp.series(javascript, reloadBrowserSync));
// }

// watch all changes
// gulp.task("watch", gulp.parallel(watchFiles, browserSync));

// default task
/*
gulp.task(
    "default",
    gulp.series(
        copyAssets,
        html,
        imageMin,
        fonts,
        css,
        javascript,
        'watch'
    ),
    function(done) {done();}
);
*/

// watch
function watch_js() {
    return gulp.watch(['frontend_redesign/js/**/*'], function(cb) {
        js_redesign();
        cb();
    })
}

function watch_scss() {
    return gulp.watch(['frontend/scss/**/*.scss'], function(cb) {
        css();
        cb();
    });
}

// build
gulp.task(
    "build",
    gulp.series(fonts,
        css,
        css_redesign,
        img,
        js_redesign,
        fonts_redesign,
        javascript)
);

gulp.task(
    "build_redesign",
    gulp.series(
        css_redesign,
        img,
        js_redesign,
        fonts_redesign
    )
)

gulp.task('js', javascript);
gulp.task('css', css);
gulp.task('fonts', fonts)
gulp.task('css_redesign', css_redesign);
gulp.task('img', img);
gulp.task('js_redesign', js_redesign);
gulp.task('fonts_redesign', fonts_redesign);
gulp.task('watch_scss', watch_scss);
gulp.task('watch_js', watch_js);