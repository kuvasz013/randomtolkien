const winston = require('winston'),
    DailyRotateFile = require('winston-daily-rotate-file'),
    moment = require('moment'),
    path = require('path'),
    config = require('../config/config');

const getNamespace = require('continuation-local-storage').getNamespace;

const LEVEL = 'info';

const smartUniJsonFormat = winston.format(function (info, opts) {
    info[Symbol.for('message')] = JSON.stringify({csinald: 1, jaja: 'aha'});
    return info;
});

const infoExtender = winston.format((info, opts) => {

    let suNS = getNamespace('hu.smartuni'),
        rid = suNS.get('rid'),
        user = suNS.get('user');

    info.timestamp = new Date();

    if (rid) {
        info.rid = rid;
    }

    if (user) {
        info.user = user.email;
    }

    return info;

});

const logger = winston.createLogger({
    level: LEVEL,
    format: winston.format.combine(
        infoExtender(),
        winston.format.json()
    ),
    transports: [
        /*
        new winston.transports.File({
            filename: './log/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: './log/combined.log'
        })
        */
    ]
});

const suFormatPrint = winston.format.printf(info => {

    let suNS = getNamespace('hu.smartuni'),
        rid = suNS.get('rid'),
        user = suNS.get('user'),
        time = moment(info.timestamp).format();

    return `${time} [${rid ? rid : '-'}][${user? user.email : '-'}] ${info.level}: ${info.message}`;
});

const suFormatPrintColorized = winston.format.printf(info => {

    let suNS = getNamespace('hu.smartuni'),
        rid = suNS.get('rid'),
        user = suNS.get('user'),
        time = moment(info.timestamp).format();

    return winston.format.colorize().colorize(info.level, `${time} [${rid ? rid : '-'}][${user? user.email : '-'}] ${info.level}: ${info.message}`)
});

const suFormatPrintJson = winston.format.printf(info => {

    let suNS = getNamespace('hu.smartuni'),
        rid = suNS.get('rid'),
        user = suNS.get('user');

    if (rid) {
        info.rid = rid;
    }

    if (user) {
        info.user = {
            id: user.id,
            email: user.email
        };
    }

    return JSON.stringify(info);
});

// Majd később különbséget teszünk logolásban környezetenként
if (true) {
    logger.add(new winston.transports.Console({
        colorize: true,
        level: LEVEL,
        format: winston.format.combine(
            winston.format.splat(),
            suFormatPrintColorized
        ),
    }));
} else {

    logger.add(new winston.transports.Console({
        colorize: true,
        format: winston.format.combine(
            winston.format.splat(),
            suFormatPrintJson
        ),
    }));

}

module.exports = logger;
