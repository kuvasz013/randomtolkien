const AWS = require('aws-sdk'),
    AWSMock = require('./mockAwsS3')
    
config = require('../config/config'),
    events = require('events'),
    logger = require('./logger'),
    _ = require('underscore');


function getAWSClient() {

    let client = {};

    // Set directory for stored files
    AWSMock.config.basePath = config.objectstore.basePath;

    client = new AWSMock.S3({
        params: { Bucket: config.objectstore.bucket },
    });

    logger.info(`ObjectStore mock created at path: ${config.objectstore.basePath}, bucket: ${config.objectstore.bucket}`);


    /*
    if (config.env === 'local') {

        // Set directory for stored files
        AWSMock.config.basePath = config.objectstore.basePath;

        client = new AWSMock.S3({
            params: { Bucket: config.objectstore.bucket },
        });
        
        logger.info(`ObjectStore mock created at path: ${config.objectstore.basePath}, bucket: ${config.objectstore.bucket}`);
        
        
        
    } else {
        client = new AWS.S3({
            endpoint: new AWS.Endpoint(config.objectstore.endpoint),
            accessKeyId: config.objectstore.key,
            secretAccessKey: config.objectstore.secret,
            params: { Bucket: config.objectstore.bucket },
        });
        logger.info(`ObjectStore connected to URL: ${config.objectstore.endpoint}, bucket: ${config.objectstore.bucket}`);
    }
    */

    return client;
}

module.exports = {
    getAWSClient
};