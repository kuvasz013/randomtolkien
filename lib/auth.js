const passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    sha1 = require('sha1'),
    // accountService = require('../app/services/accountService'),
    USER_STATUS = require('../app/enums/user_status'),
    logger = require('./logger'),
    userRepository = require('../app/repositories/userRepository');


/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */
passport.use(new LocalStrategy(
    async function(username, password, done) {
        logger.debug('Login user: %s / %s', username, password);


        let users = await userRepository.find({username: username});
        let user = users.length == 1 ? users[0] : null;

        if (!user) {
            logger.debug("No user");
            return done(null, false);
        }

        if (user.status != null) {
            logger.debug("User has disabled status: " + user.status);
            return done(null, false);
        }

        if (user.password !== sha1(password)) {
            logger.debug("Wrong password: ", user.password, password);
            return done(null, false);
        }

        logger.debug("Logged in with local strategy: %s", user.username);
        try {
            await userRepository.update({
                id: user.id,
                last_login: new Date()
            });
        } catch(e) {
            logger.error('Error saving last login date %j', e);
        }

        return done(null, user);

    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(async function(id, done) {

    let users = await userRepository.find({id});
    done(null, users.length == 1 ? users[0] : null);
    

});
