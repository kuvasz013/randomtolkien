// Minimal i18n lib, if we need a more complex one in the future: https://www.npmjs.com/package/i18n

const ERROR = require('../app/enums/error');

let dict = {
    'LESSON_STATUS_ready': 'kezdésre kész',
    'LESSON_STATUS_inprogress': 'folyamatban',
    'LESSON_STATUS_finished': 'lezárva',
    'LESSON_STATUS_deleted': 'törölt',
    'admin': 'adminisztrátor',
    'agent': 'megbízott',
    'teacher': 'tanár',
    'student': 'tanuló'
}

dict[`${ERROR.WRONG_EMAIL_OR_PASSWORD}`] = 'Hibás felhasználónév vagy jelszó!';
dict[`${ERROR.COURSE_NOT_FOUND}`] = 'A kurzus nem található!';
dict[`${ERROR.REGISTRATION_ERROR}`] = 'Regisztrációs hiba, kérjük próbálja újra később!';
dict[`${ERROR.EMAIL_ALREADY_EXISTS}`] = 'A megadott email cím már foglalt!';

function translate(text) {
    return dict[text] || text;
}


module.exports = {
    translate
}