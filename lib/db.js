const config = require('../config/config'),
    logger = require('./logger'),
    knex = require("knex")({
        client: 'pg',
        connection: {
            host : config.database.host,
            user : config.database.username,
            password : config.database.password,
            database : config.database.database
        },
        searchPath: [config.database.schema],
        debug: false,
        pool: {
            afterCreate: function (conn, done) {
                // in this example we use pg driver's connection API
                logger.info(`Database connected: ${config.database.host}`);
                done(null, conn);
            }
        },
        log: {
            warn(message) {
                logger.warn("DB log: %s", message);
            },
            error(message) {
                logger.error("DB log: %s", message);
            },
            deprecate(message) {
                logger.warn("DB log: %s", message);
            },
            debug(message) {
                logger.debug("DB log: %s", message);
            },
          }
    });

module.exports = knex;