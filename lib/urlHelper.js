const config = require('../config/config');


function panel(path) {
    return create('panel', path);
}

function landing(path) {
    return create('landing', path);
}

function create(app, path) {
    return config.proto + '://' + config[app + 'Host'] + path;
}



module.exports = {
    panel,
    landing,
    create
};