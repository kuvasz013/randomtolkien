module.exports = {
    apps: [
      {
        name: 'randomtolkien',
        script: 'app/index.js',
        watch: false,
        ignore_watch: ['node_modules'],
        env: {
          NODE_ENV: 'deploy'
        }
      }
    ],
  };