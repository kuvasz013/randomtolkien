# SmartUni Server

## Start steps
```
$ npm install
$ docker-compose up
$ npm run db:migrate
$ npm run frontend:build
$ npm run serve
```
- Open http://localhost:6500

## Frontend
- Template from: https://coderthemes.com/minton/layouts/vertical/blue/index.html
- Source is in ```/frontend```
- Build JS/CSS files with: ```npm run frontend:build```
- Destination folder: ```/public```

## Backend
- ExpressJS app with virtual hosts (only using panel app right now)
- Config file in ```/config```

App structure:
```
/app
    index.js  // server init
    /app
        /server/app.js   // app init (path bindings, view init, etc.)
        /controllers
        /enums
        /repositories
        /services
        /views
        
/lib    // common stuff
```

## Database migration
- We use ```db-migrate``` package
- Readme file in ```/db``` folder

## Release
- Every push to the master branch triggers a hook and the master branch will be release to the dev server in seconds
